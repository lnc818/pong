import React from 'react';
import { View, StyleSheet, Slider, ScrollView  } from 'react-native';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';

import { Text, Button, Paragraph, Dialog, Portal, FAB, TextInput, Chip, Switch, Avatar  } from 'react-native-paper';

import Dimensions from 'Dimensions';

import { Col, Row, Grid } from "react-native-easy-grid";

import { connect } from "react-redux";

import { add_room } from '../../actions'

let ScreenHeight = Dimensions.get("window").height;

const styles = StyleSheet.create({
  fab: {
    position: 'absolute',
    margin: 16,
    right: 0,
    bottom: 80,
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
    flexDirection: 'row',
  },
  leftPane: {
    flex: 1,
    // backgroundColor: '#33373B',
    padding: 16,
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  rightPane: {
    flex: 2,
    padding: 16,
    // backgroundColor: '#33373B',
  },
  chip: {
    margin: 4,
  },
});
class AddHistoryDialog extends React.Component {

  
  constructor(props){


    super(props);
    this.state = {
      text:'',
      visible: false,
      winner:null,
      loser:null,
      size:3,
      is_one_lose_all: false,
      selectedItems: [],
    };
  }
  componentDidMount() {
    this.props.setClickAddHistoryDialog(this._showDialog);
 }

  onSelectedItemsChange = (selectedItems) => {
    console.log('selectedItems',selectedItems)
    this.setState({ selectedItems });
  };

  _showDialog = () => {this.setState({ visible: true })};

  _hideDialog = () => {this.setState({ visible: false })};

  _addHistory = () => {
    let history = {
      room_uid: this.props.room.current_room.uid,
      winner:this.state.winner,
      loser:this.state.loser,
      size: this.state.size,
      is_one_lose_all: this.state.is_one_lose_all,
    }
    this.props.add_history(this.props.user, this.props.room.current_room, history);
    this._hideDialog();
  }

  _minChanged = (value) => {
    if(value >= this.state.max)
      value = this.state.max;
    this.setState({min:value});
  }
  _maxChanged = (value) => {
    if(value <= this.state.min)
      value = this.state.min;
    this.setState({max:value});
  }
  _sizeChanged = (index, value) => {
    var arr = this.state.size_arr;
    arr[index] = value
    this.setState({size_arr:arr});
  }

  _winnerChanged = (winner_uid)=>{
    if(this.state.winner == winner_uid)
      this.setState({winner:null});  
    else
      this.setState({winner:winner_uid});
  }
  _loserChanged = (loser_uid)=>{
    if(this.state.loser == loser_uid)
      this.setState({loser:null});  
    else
      this.setState({loser:loser_uid});
  }



  renderWinner = () => {
    var self = this;
    let room = self.props.room;
    return (
      <View style={styles.container}>
        <View style={styles.leftPane} >
          <Text>winner</Text>
        </View>

        <View style={styles.rightPane}>
          {
            room.current_room_users.map((item)=>{
              return(
                <Chip
                  selected={self.state.winner == item.uid}
                  avatar={<Avatar.Image source={{ uri: item.photoURL }} />}
                  onPress={() => self._winnerChanged(item.uid)}
                  style={styles.chip}
                >
                  {`${item.displayName}${self.state.winner == item.uid? 'selected': ''}`}
                </Chip>
              )
            })
          }
        </View>
      </View>
    )
  }
  renderLoser = () => {
    var self = this;
    let room = self.props.room;
    return (
      <View style={styles.container}>
        <View style={styles.leftPane} >
          <Text>loser</Text>
        </View>

        <View style={styles.rightPane}>
          {
            room.current_room_users.map((item)=>{
              
              return(
                <Chip
                  selected={self.state.loser == item.uid}
                  avatar={<Avatar.Image source={{ uri: item.photoURL }} />}
                  onPress={() => self._loserChanged(item.uid)}
                  style={styles.chip}
                >
                  {`${item.displayName}${self.state.loser == item.uid? 'selected': ''}`}
                </Chip>
              )
            })
          }
          <Chip
            selected={self.state.loser == 0}
            // avatar={<Image source={{ uri: item.photoURL }} />}
            onPress={() => self._loserChanged(0)}
            style={styles.chip}
          >
            {`All + ${self.state.loser == 0? 'selected': ''}`}
          </Chip>
        </View>
      </View>
    )
  }
      render() {
        var self = this;
        return (
            <Portal>
              <Dialog
                  style={{maxHeight:'80%'}}
                  visible={this.state.visible}
                  onDismiss={this._hideDialog}>
                <Dialog.ScrollArea>
                  <ScrollView 
                    // contentContainerStyle={{ paddingHorizontal: 24 }}
                  >
                    <Dialog.Title>New History</Dialog.Title>
                    <Dialog.Content>
                      <Paragraph>Please fill in below to add history</Paragraph>
                      {this.renderWinner()}
                      {this.renderLoser()}
                      <View style={styles.container}>
                        <View style={styles.leftPane} >
                          <Text>Size: {this.state.size}</Text>
                        </View>
                        <View style={styles.rightPane}>
                          <Slider
                            step={1}
                            minimumValue={1}
                            maximumValue={13}
                            minimumTrackTintColor="#456534"
                            maximumTrackTintColor="#000000"
                            value={this.state.size}
                            onValueChange={(value)=>this._sizeChanged(value)}
                          />
                        </View>
                      </View>
                      <View style={styles.container}>
                        <View style={styles.leftPane} >
                          <Text>One lose all: {this.state.size}</Text>
                        </View>
                        <View style={styles.rightPane}>
                          <Switch
                            value={this.state.is_one_lose_all}
                            onValueChange={() =>
                              { this.setState({ is_one_lose_all: !this.state.is_one_lose_all }); }
                            }
                          />
                        </View>
                      </View>

                    </Dialog.Content>
                    <Dialog.Actions>
                      <Button onPress={this._addRoom}>Done</Button>
                    </Dialog.Actions>
                  </ScrollView>
                </Dialog.ScrollArea>
              </Dialog>
              {/* <FAB.Group
                open={this.state.open}
                icon={this.state.open ? 'today' : 'add'}
                actions={[
                  { icon: 'casino', label: 'room', onPress:()=>this._showDialog()},
                  { icon: 'people', label: 'member', onPress:()=>this._showDialog()},

                ]}
                onStateChange={({ open }) => this.setState({ open })}
                onPress={() => {
                  if (this.state.open) {
                    // do something if the speed dial is open
                  }
                }}
              /> */}
              {/* <FAB
                style={styles.fab}
                small
                icon="add"
                onPress={()=>this._showDialog()}
              /> */}
            </Portal>
        );
      }
    }
    const mapStateToProps = state => ({
      loading: state.auth.loading,
      user: state.auth.user,
      group: state.group,
      room: state.room,
    });
    const mapDispatchToProps = {
        add_room: add_room,
    };
    export default connect(mapStateToProps,mapDispatchToProps)(AddHistoryDialog);