import React from 'react';
import { View, StyleSheet, Slider, ScrollView  } from 'react-native';

import { Text, Button, Paragraph, Dialog, Portal, FAB, TextInput   } from 'react-native-paper';

import Dimensions from 'Dimensions';

import { Col, Row, Grid } from "react-native-easy-grid";

import { connect } from "react-redux";

import { add_guest } from '../../actions'

let ScreenHeight = Dimensions.get("window").height;

class AddPlayerDialog extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      name:'',
      visible: false,
    };
  }
  componentDidMount() {
    this.props.setClickAddMemberDialog(this._showDialog);
 }
  _showDialog = () => {this.setState({ visible: true })};

  _hideDialog = () => {this.setState({ visible: false })};

  _addGuest = () => {
    this.props.add_guest(this.props.user, this.state.name);
    this._hideDialog();
  }
      render() {
        var self = this;
        return (
            <Portal>
              <Dialog
                  style={{maxHeight:'80%'}}
                  visible={this.state.visible}
                  onDismiss={this._hideDialog}>
                <Dialog.ScrollArea>
                  <ScrollView 
                    // contentContainerStyle={{ paddingHorizontal: 24 }}
                  >
                    <Dialog.Title>New Player</Dialog.Title>
                    <Dialog.Content>
                      <Paragraph>Please fill in below to add group</Paragraph>
                      <TextInput
                        label='Name'
                        mode='outlined'
                        value={this.state.name}
                        onChangeText={text => this.setState({ name: text})}
                      />

                    </Dialog.Content>
                    <Dialog.Actions>
                      <Button onPress={this._addGuest}>Done</Button>
                    </Dialog.Actions>
                  </ScrollView>
                </Dialog.ScrollArea>
              </Dialog>
            </Portal>
        );
      }
    }
    const mapStateToProps = state => ({
      loading: state.auth.loading,
      user: state.auth.user
    });
    const mapDispatchToProps = {
      add_guest: add_guest,
    };
    export default connect(mapStateToProps,mapDispatchToProps)(AddPlayerDialog);