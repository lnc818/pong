import React from 'react';

import { View, StyleSheet, TouchableHighlight } from 'react-native';

import { Text, Title, Avatar } from 'react-native-paper';
import { systemWeights, human, iOSColors  } from 'react-native-typography';
// import {
//   LineChart,
// } from 'react-native-chart-kit'

import { connect } from "react-redux";

import { ThinGrayLine, ThickGrayLine, ThickDarkGrayLine, ThinRedLine,} from '../../Lines';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    flexDirection: 'column',
  },
  rowPanel: {
    flex: 1,
    backgroundColor: '#33373B',
    padding: 16,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});
const data = [
  { quarter: 1, earnings: 13000 },
  { quarter: 2, earnings: 16500 },
  { quarter: 3, earnings: 14250 },
  { quarter: 4, earnings: 19000 }
];
class PlayerChart extends React.Component {
  
  constructor(props) {
    super(props);

    this.state = {
      expanded: false,
    };
  }
  _getTextColor= (num) => {
    if(num>0)
    {
      return iOSColors.green;
    }
    else if(num<0)
    {
      return iOSColors.red;
    }
    else
      return iOSColors.lightGray;
  }
  render(){
    console.log('render playerdetailcard')
    console.log(this.props)
    let {player_details} = this.props;
    const contentInset = { left: 10, right: 10, top: 10, bottom: 7 };
    return (
      <TouchableHighlight style={styles.container} onPress={this.props.onPress}>
        <View style={styles.container}>
          {/* <LineChart
            data={{
            labels: ['January', 'February', 'March', 'April', 'May', 'June'],
            datasets: [{
              data: [
                Math.random() * 100,
                Math.random() * 100,
                Math.random() * 100,
                Math.random() * 100,
                Math.random() * 100,
                Math.random() * 100
              ]
            }]
            }}
            // width={Dimensions.get('window').width} // from react-native
            height={220}
            yAxisLabel={'$'}
            chartConfig={{
              backgroundColor: '#e26a00',
              backgroundGradientFrom: '#fb8c00',
              backgroundGradientTo: '#ffa726',
              decimalPlaces: 2, // optional, defaults to 2dp
              color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
              style: {
                borderRadius: 16
              }
            }}
            bezier
            style={{
              marginVertical: 8,
              borderRadius: 16
            }}
          /> */}
        </View>
      </TouchableHighlight>
    )
  }
}
const mapStateToProps = state => {
  return {
  loading: state.auth.loading,
  user: state.auth.user,
  groups: state.group.groups,
  rooms: state.room.rooms
}};
const mapDispatchToProps = {
  // add_group: add_group,
};
export default connect(mapStateToProps,mapDispatchToProps)(PlayerChart)