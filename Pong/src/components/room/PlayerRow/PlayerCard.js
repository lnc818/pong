import React from 'react';

import { View, StyleSheet, TouchableHighlight } from 'react-native';

import { Text, Title, Avatar } from 'react-native-paper';
import { human } from 'react-native-typography'

import { connect } from "react-redux";

import { ThinGrayLine, ThickGrayLine, ThickDarkGrayLine, ThinRedLine,} from '../../Lines';


import avatar1 from '../../../images/avatar/a1.png';
import avatar2 from '../../../images/avatar/a2.png';
import avatar3 from '../../../images/avatar/a3.png';
import avatar4 from '../../../images/avatar/a4.png';


let avatars = [avatar1,avatar2,avatar3,avatar4]

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    flexDirection: 'row',
  },
  leftPane: {
    flex: 1,
    backgroundColor: '#33373B',
    padding: 16,
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  rightPane: {
    flex: 1,
    padding: 16,
    backgroundColor: '#33373B',
  },
});

class PlayCarder extends React.Component {
  
  constructor(props) {
    super(props);

    this.state = {
      expanded: false,
    };
  }

  render(){
    var self = this;
    var avatat_path = `../../../images/avatar/a${self.props.player_number}.png`;
    avatat_path = 1;
    console.log('render playerrow')
    console.log(this.props)
    return (
      <TouchableHighlight style={styles.container} onPress={this.props.onPress}>
      <View style={styles.container}>
        <View style={styles.leftPane} >
          <Avatar.Image size={60} source={self.props.player.photoURL == ''? avatars[self.props.player_number-1] : { uri: self.props.player.photoURL}} />
          <Title style={human.title2White}>{self.props.player.displayName}</Title>
        </View>

      <View style={styles.rightPane}>
        <View style={{ flex: 1, flexDirection: 'column',justifyContent: 'center',alignItems: 'center', }}>
          <Title style={human.title1White}>{this.props.player_current_score}</Title>
        </View>
      </View>

    </View>
    </TouchableHighlight>
    )
  }
}
const mapStateToProps = state => {
  return {
  loading: state.auth.loading,
  user: state.auth.user,
  groups: state.group.groups,
  rooms: state.room.rooms
}};
const mapDispatchToProps = {
  // add_group: add_group,
};
export default connect(mapStateToProps,mapDispatchToProps)(PlayCarder)