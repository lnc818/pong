import React from 'react';

import { View, StyleSheet, TouchableHighlight } from 'react-native';

import { Text, Title, Avatar } from 'react-native-paper';
import { systemWeights, human, iOSColors  } from 'react-native-typography'

import { connect } from "react-redux";

import { ThinGrayLine, ThickGrayLine, ThickDarkGrayLine, ThinRedLine,} from '../../Lines';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    flexDirection: 'column',
  },
  rowPanel: {
    flex: 1,
    backgroundColor: '#33373B',
    padding: 16,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});
class PlayerDetailCard extends React.Component {
  
  constructor(props) {
    super(props);

    this.state = {
      expanded: false,
    };
  }
  _getTextColor= (num) => {
    if(num>0)
    {
      return iOSColors.green;
    }
    else if(num<0)
    {
      return iOSColors.red;
    }
    else
      return iOSColors.lightGray;
  }
  render(){
    console.log('render playerdetailcard')
    console.log(this.props)
    let {player_details} = this.props;
    return (
      <TouchableHighlight style={styles.container} onPress={this.props.onPress}>
        <View style={styles.container}>
          <View style={styles.rowPanel} >
            <View style={{ flex: 1, flexDirection: 'column' }}>
              <Text style={{...human.caption1White,...systemWeights.light}}>highest: </Text>
            </View>
            <View style={{ flex: 1, flexDirection: 'column' }}>
              <Text style={{...human.caption1White,...systemWeights.light,color: this._getTextColor(this.props.player_details.highest)}}>{this.props.player_details.highest}</Text>
            </View>
            <View style={{ flex: 1, flexDirection: 'column' }}>
              <Text style={{...human.caption1White,...systemWeights.light}}>lowest</Text>
            </View>
            <View style={{ flex: 1, flexDirection: 'column' }}>
              <Text style={{...human.caption1White,...systemWeights.light,color: this._getTextColor(this.props.player_details.lowest)}}>{this.props.player_details.lowest}</Text>
            </View>
            <View style={{ flex: 1, flexDirection: 'column' }}>
              <Text style={{...human.caption1White,...systemWeights.light}}>mean: </Text>
            </View>
            <View style={{ flex: 1, flexDirection: 'column' }}>
              <Text style={{...human.caption1White, ...systemWeights.light,color: this._getTextColor(this.props.player_details.mean)}}>{this.props.player_details.mean}</Text>
            </View>
          </View>
          <View style={styles.rowPanel} >
            <View style={{ flex: 1, flexDirection: 'column' }}>
              <Text style={{...human.caption1White,...systemWeights.light}}>mean: </Text>
            </View>
            <View style={{ flex: 1, flexDirection: 'column' }}>
              <Text style={{...human.caption1White,...systemWeights.light,color: this._getTextColor(this.props.player_details.mean)}}>{this.props.player_details.mean}</Text>
            </View>
            <View style={{ flex: 1, flexDirection: 'column' }}>
              <Text style={{...human.caption1White,...systemWeights.light}}>lowest</Text>
            </View>
            <View style={{ flex: 1, flexDirection: 'column' }}>
              <Text style={{...human.caption1White,...systemWeights.light,color: this._getTextColor(this.props.player_details.lowest)}}>{this.props.player_details.lowest}</Text>
            </View>
          </View>
        </View>
      </TouchableHighlight>
    )
  }
}
const mapStateToProps = state => {
  return {
  loading: state.auth.loading,
  user: state.auth.user,
  groups: state.group.groups,
  rooms: state.room.rooms
}};
const mapDispatchToProps = {
  // add_group: add_group,
};
export default connect(mapStateToProps,mapDispatchToProps)(PlayerDetailCard)