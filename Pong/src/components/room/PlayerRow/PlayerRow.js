import React from 'react';
  
import { LayoutAnimation, UIManager, View,} from 'react-native';
import { Button, Paragraph, Dialog, Portal, FAB, List, Appbar, Avatar, Card, Title, DataTable   } from 'react-native-paper';

import { connect } from "react-redux";

import FoldView from 'react-native-foldview';

import PlayerCard from './PlayerCard';
import PlayerDetailCard from './PlayerDetailCard';
import PlayerChart from './PlayerChart';
import ProfileCard from '../../foldview/components/ProfileCard';
  
  // Enable LayoutAnimation on Android
  if (UIManager.setLayoutAnimationEnabledExperimental) {
    UIManager.setLayoutAnimationEnabledExperimental(true);
  }
  
  const ROW_HEIGHT = 150;
  
  const Spacer = ({ height }) => (
    <View
      pointerEvents="none"
      style={{
        height,
      }}
    />
  );
  
  class PlayerRow extends React.Component {
  
    constructor(props) {
      super(props);
  
      this.state = {
        expanded: false,
        height: ROW_HEIGHT,
      };
    }
  
    componentWillMount() {
      this.flip = this.flip.bind(this);
      this.handleAnimationStart = this.handleAnimationStart.bind(this);
      this.renderFrontface = this.renderFrontface.bind(this);
      this.renderBackface = this.renderBackface.bind(this);
    }
  
    flip() {
      this.setState({
        expanded: !this.state.expanded,
      });
    }
  
    handleAnimationStart(duration, height) {
      const isExpanding = this.state.expanded;
  
      const animationConfig = {
        duration,
        update: {
          type: isExpanding ? LayoutAnimation.Types.easeOut : LayoutAnimation.Types.easeIn,
          property: LayoutAnimation.Properties.height,
        },
      };
  
      LayoutAnimation.configureNext(animationConfig);
  
      this.setState({
        height,
      });
    }
  
    renderFrontface() {
      return (
        <PlayerCard 
          onPress={this.flip} 
          player_number={this.props.player_number}
          player_uid={this.props.player_uid} 
          player={this.props.player} 
          player_current_score = {this.props.player_current_score}
        />
      );
    }
  
    renderBackface() {
      return (
        <PlayerChart 
          onPress={this.flip} 
          player_number={this.props.player_number}
          player_uid={this.props.player_uid} 
          player={this.props.player}
          player_details={this.props.player_details}
          player_current_score = {this.props.player_current_score}
          player_histories={this.props.player_histories}
        />
        // <ProfileCard onPress={this.flip} />
      );
    }
  
    render() {
      const { height } = this.state;
      const { zIndex } = this.props;
  
      const spacerHeight = height - ROW_HEIGHT;
  
      return (
        <View
          style={{
            flex: 1,
            zIndex,
            // marginBottom: this.state.expanded ? 150 : 0
          }}
        >
          <View
            style={{
              height: ROW_HEIGHT,
              margin: 10,
            }}
          >
            <FoldView
              expanded={this.state.expanded}
              onAnimationStart={this.handleAnimationStart}
              perspective={1000}
              renderBackface={this.renderBackface}
              renderFrontface={this.renderFrontface}
            >
              <PlayerDetailCard onPress={this.flip} 
                player_uid={this.props.player_uid} 
                player={this.props.player} 
                player_details={this.props.player_details}
              />
            </FoldView>
  
          </View>
  
          <Spacer height={spacerHeight} />
        </View>
      );
    }
  }
  const mapStateToProps = state => ({
    loading: state.auth.loading,
    user: state.auth.user,
    groups: state.group.groups,
    rooms: state.room.rooms
  });
const mapDispatchToProps = {
    // add_group: add_group,
};
export default connect(mapStateToProps,mapDispatchToProps)(PlayerRow)