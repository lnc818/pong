import React from 'react';
import { Text, View, StyleSheet  } from 'react-native';
import { Button, Paragraph, Dialog, Portal, FAB, TextInput,Provider   } from 'react-native-paper';
import Dimensions from 'Dimensions';

import { connect } from "react-redux";

import { add_group } from '../../actions'


let ScreenHeight = Dimensions.get("window").height;

const styles = StyleSheet.create({
  fab: {
    position: 'absolute',
    margin: 16,
    right: 0,
    bottom: 80,
  },
})
class AddGroupDialog extends React.Component {

  
  constructor(props){


    super(props);
    this.state = {
      text:'',
      visible: false,
    };
  }
  componentDidMount() {
    this.props.setClickAddGroupDialog(this._showDialog);
 }

  getAlert() {
    console.log('getAlert from Child');
  }

  _showDialog = () => {this.setState({ visible: true })};

  _hideDialog = () => {this.setState({ visible: false })};

  _addGroup = () => {
    this.props.add_group(this.state.name, this.props.user);
    this._hideDialog();
  }
      render() {
        return (
          <Portal>
              <Dialog
                 visible={this.state.visible}
                 onDismiss={this._hideDialog}>
                <Dialog.Title>New Group</Dialog.Title>
                <Dialog.Content>
                  <Paragraph>Please fill in below to add group</Paragraph>
                  <TextInput
                    label='Name'
                    mode='outlined'
                    value={this.state.name}
                    onChangeText={text => this.setState({ name: text})}
                  />
                </Dialog.Content>
                <Dialog.Actions>
                  <Button onPress={this._addGroup}>Done</Button>
                </Dialog.Actions>
              </Dialog>
              {/* <FAB
                style={styles.fab}
                small
                icon="add"
                onPress={()=>this._showDialog()}
              /> */}
          </Portal>
        );
      }
    }
    const mapStateToProps = state => ({
      loading: state.auth.loading,
      user: state.auth.user
    });
    const mapDispatchToProps = {
        add_group: add_group,
    };
    export default connect(mapStateToProps,mapDispatchToProps)(AddGroupDialog);