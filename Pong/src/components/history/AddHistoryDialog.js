import React from 'react';
import { View, StyleSheet, Slider, ScrollView  } from 'react-native';

import { Text, Button, Paragraph, Dialog, Portal, FAB, TextInput   } from 'react-native-paper';

import Dimensions from 'Dimensions';

import { Col, Row, Grid } from "react-native-easy-grid";

import { connect } from "react-redux";

import { add_room } from '../../actions'

let ScreenHeight = Dimensions.get("window").height;

const size_arr_init = [2,4,8,16,24,32,48,64,96,128,196,256,512]
const styles = StyleSheet.create({
  fab: {
    position: 'absolute',
    margin: 16,
    right: 0,
    bottom: 80,
  },
})
class AddHistoryDialog extends React.Component {

  
  constructor(props){


    super(props);
    this.state = {
      text:'',
      visible: false,
      min:3,
      max:10,
      size_arr:size_arr_init.map((item)=>item)
    };
  }
  componentDidMount() {
    this.props.setClickAddRoomDialog(this._showDialog);
 }
  _showDialog = () => {this.setState({ visible: true })};

  _hideDialog = () => {this.setState({ visible: false })};

  _addRoom = () => {
    this.props.add_room(this.state.name, this.props.user);
    this._hideDialog();
  }

  _minChanged = (value) => {
    if(value >= this.state.max)
      value = this.state.max;
    this.setState({min:value});
  }
  _maxChanged = (value) => {
    if(value <= this.state.min)
      value = this.state.min;
    this.setState({max:value});
  }
  _sizeChanged = (index, value) => {
    var arr = this.state.size_arr;
    arr[index] = value
    this.setState({size_arr:arr});
  }
  _render_size = (start,end) => {
    var self = this;
    return (
     <Row>
       {
         Array(this.state.max - this.state.min + 1).fill().map((x,i)=>{
           if(i<start || i>=end)
           return
           var index = i+this.state.min-1;
           var val = self.state.size_arr[index];
           val = (val).toString();
           console.log(`size_arr[${index}]`,val);
           return(
             <Col>
             <TextInput
               label={index+1}
               keyboardType='number-pad'
               dense={true}
               // mode='outlined'
               // value={this.state.size_arr[index]}
               value={val}
               onChangeText={number => this._sizeChanged(index,number)}
             />
             </Col>
           )
         })
       }
     </Row>
    )
  }
      render() {
        var self = this;
        return (
            <Portal>
              <Dialog
                  style={{maxHeight:'80%'}}
                  visible={this.state.visible}
                  onDismiss={this._hideDialog}>
                <Dialog.ScrollArea>
                  <ScrollView 
                    // contentContainerStyle={{ paddingHorizontal: 24 }}
                  >
                    <Dialog.Title>New Room</Dialog.Title>
                    <Dialog.Content>
                      <Paragraph>Please fill in below to add group</Paragraph>
                      <TextInput
                        label='Name'
                        mode='outlined'
                        value={this.state.name}
                        onChangeText={text => this.setState({ name: text})}
                      />
                      <Text>Min: {this.state.min}</Text>
                      <Slider
                        step={1}
                        minimumValue={1}
                        maximumValue={13}
                        minimumTrackTintColor="#456534"
                        maximumTrackTintColor="#000000"
                        value={this.state.min}
                        onValueChange={(value)=>this._minChanged(value)}
                      />
                      <Text>Max: {this.state.max}</Text>
                      <Slider
                        step={1}
                        minimumValue={1}
                        maximumValue={13}
                        minimumTrackTintColor="#456534"
                        maximumTrackTintColor="#000000"
                        value={this.state.max}
                        onValueChange={(value)=>this._maxChanged(value)}
                      />
                      <Grid>
                        {self._render_size(0,4)}
                        {self._render_size(4,8)}
                        {self._render_size(8,12)}
                        {self._render_size(12,13)}
                      </Grid>
                    </Dialog.Content>
                    <Dialog.Actions>
                      <Button onPress={this._addRoom}>Done</Button>
                    </Dialog.Actions>
                  </ScrollView>
                </Dialog.ScrollArea>
              </Dialog>
              {/* <FAB.Group
                open={this.state.open}
                icon={this.state.open ? 'today' : 'add'}
                actions={[
                  { icon: 'casino', label: 'room', onPress:()=>this._showDialog()},
                  { icon: 'people', label: 'member', onPress:()=>this._showDialog()},

                ]}
                onStateChange={({ open }) => this.setState({ open })}
                onPress={() => {
                  if (this.state.open) {
                    // do something if the speed dial is open
                  }
                }}
              /> */}
              {/* <FAB
                style={styles.fab}
                small
                icon="add"
                onPress={()=>this._showDialog()}
              /> */}
            </Portal>
        );
      }
    }
    const mapStateToProps = state => ({
      loading: state.auth.loading,
      user: state.auth.user
    });
    const mapDispatchToProps = {
        add_room: add_room,
    };
    export default connect(mapStateToProps,mapDispatchToProps)(AddHistoryDialog);