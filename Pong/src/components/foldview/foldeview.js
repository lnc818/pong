import React from 'react';
import {Text, Platform, ScrollView, StatusBar, StyleSheet, View,} from 'react-native';
import { Button, Paragraph, Dialog, Portal, FAB, List, Appbar, Avatar, Card, Title, DataTable   } from 'react-native-paper';
import { connect } from "react-redux";

import Dimensions from 'Dimensions';

// import AddHistoryDialog from '../components/room/AddHistoryDialog'



import Row from './Row';

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : 0;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: '200%'
  },
  scrollView: {
    backgroundColor: '#4A637D',
    flex: 1,
    padding: 10,
    paddingTop: STATUSBAR_HEIGHT,
    height: '200%'
  },
});
class foldview extends React.Component {
    constructor(props){


        super(props);
        this.state = {
         visible: false,
         fabOpen:true,
         expanded: false,
        };
    }
    render(){
        return(
            <View style={styles.container}>
              {/* <StatusBar
                barStyle="light-content"
              /> */}
              <ScrollView
                style={styles.scrollView}
              >
                <Row zIndex={100} />
                <Row zIndex={90} />
                <Row zIndex={80} />
                <Row zIndex={70} />
              </ScrollView>
            </View>
        )
    }
}

const mapStateToProps = state => ({
    loading: state.auth.loading,
    user: state.auth.user,
    groups: state.group.groups,
    rooms: state.room.rooms
  });
const mapDispatchToProps = {
    // add_group: add_group,
};
export default connect(mapStateToProps,mapDispatchToProps)(foldview);