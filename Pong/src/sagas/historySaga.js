import { put, takeLatest, all, call, takeEvery, select } from 'redux-saga/effects';
import axios from 'axios';

import { Facebook } from 'expo';

import { config } from '../config';
import { Firebase } from '../integrations/firebase';

import apiConfig from '../config/api'

import AuthService from '../services/Auth';

const getUser = state => state.auth.user;
const getCurrentGroup = state => {if(state.group == null) return null; else return state.group.current_group};
const getCurrentRoom = state => {if(state.room == null) return null; else return state.room.current_room};

////get histories
function getHistoriesApi(user, current_room) {
  console.log('getHistorysApi');
  console.log(user, current_room);
  return axios(
    {
      method: 'post',
      url: `${apiConfig.apiPath}/api/history/GetHistories`,
      data: {user:user,current_room: current_room}
    }
  )
  .then(response => ({response: response.data}))
  .catch(err => {
    console.log('err')
    console.log(err)
    return ({error: err})
  });
}

function* getHistoriesSaga(action) {
    console.log('getHistoriesSaga')
    console.log(action);
    let { response, error } = yield call(getHistoriesApi, action.payload.user, action.payload.current_room);
    console.log(response);
    if(response)
        yield put({type: "GET_HISTORIES_SUCCESS", payload:{histories: response}})
    else
        yield put({type: "GET_HISTORIES_FAIL"})
}
export function* getHistoriesWatch() {
  yield takeLatest("GET_HISTORIES", getHistoriesSaga);
}
////

////add history
function addHistoryApi(user, current_room, history) {
  console.log('addHistoriesApi');
  console.log(user, current_room,history);
  return axios(
    {
      method: 'post',
      url: `${apiConfig.apiPath}/api/history/AddHistory`,
      data: {user:user,current_room:current_room,history:history}
    }
  )
  .then(response => ({response: response.data}))
  .catch(err => {
    console.log('err')
    console.log(err)
    return ({error: err})
  });
}
function* addHistorySaga(action) {
  console.log('addHistorySaga');

  var user_firebase = yield Firebase.auth().currentUser;
  const user = yield select(getUser);
  const current_room = yield select(getCurrentRoom);
  var idToken = yield user_firebase.getIdToken(false);
  user.idToken = idToken;
  console.log(user);

  let { response, error } = yield call(addHistoryApi, action.payload.user, current_room, action.payload.history);
  console.log(response);
    if(response)
        yield put({type: "ADD_HISTORY_SUCCESS"})
    else
        yield put({type: "ADD_HISTORY_FAIL"})

  yield put({type: "GET_HISTORIES", payload:{user: user, current_room: current_room}})
}
export function* addHistoryWatch() {
  yield takeLatest("ADD_HISTORY", addHistorySaga);
}
////