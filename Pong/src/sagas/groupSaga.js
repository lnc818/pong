import { select, put, takeLatest, all, call, takeEvery } from 'redux-saga/effects';
import axios from 'axios';

import { Facebook } from 'expo';

import { config } from '../config';
import { Firebase } from '../integrations/firebase';

import apiConfig from '../config/api'

import AuthService from '../services/Auth';

import NavigationService from '../services/NavigationService'

const getUser = state => state.auth.user;
const getCurrentGroup= state => {if(state.group == null) return null; else return state.group.current_group};

///enter group
function* enterGroupSaga(action) {
  console.log('enterGroupSaga');
  yield NavigationService.navigate('Group');
  var user_firebase = yield Firebase.auth().currentUser;

  const user = yield select(getUser);
  var idToken = yield user_firebase.getIdToken(false);
  user.idToken = idToken;
  console.log(user);
  yield put({type: "GET_ROOMS", payload:{user: user, current_group: action.payload.current_group}})
  yield put({type: "GET_GROUP_USERS", payload:{user: user, current_group: action.payload.current_group}})
}
export function* enterGroupWatch() {
  yield takeLatest("ENTER_GROUP", enterGroupSaga);
}
////

////get group's user
function getGroupUsersApi(user, current_group) {
  console.log('getGroupUsersApi');
  console.log(user, current_group);
  return axios(
    {
      method: 'post',
      url: `${apiConfig.apiPath}/api/group/GetUsersByGroup`,
      data: {user:user,current_group:current_group}
    }
  )
  .then(response => {
    console.log('getGroupUsersApi success');
    console.log(response.data)
    return {response: response.data}
  })
  .catch(err => {
    console.log('err')
    console.log(err)
    return ({error: err})
  });
}
function* getGroupUsersSaga(action) {
  console.log('enterGroupSaga');

  var user_firebase = yield Firebase.auth().currentUser;

  const user = yield select(getUser);
  var idToken = yield user_firebase.getIdToken(false);
  user.idToken = idToken;
  const current_group = yield select(getCurrentGroup);

  let { response, error } = yield call(getGroupUsersApi, user, current_group);
  console.log(response);
  if(response)
    yield put({type: "GET_GROUP_USERS_SUCCESS", payload:{current_group_users: response}})
  else
    yield put({type: "GET_GROUP_USERS_FAIL"})
}
export function* getGroupUsersWatch() {
  yield takeLatest("GET_GROUP_USERS", getGroupUsersSaga);
}
////

////add user
function addMemberApi(user, guest, current_group) {
  console.log('getRoomsApi');
  console.log(user, guest);
  return axios(
    {
      method: 'post',
      url: `${apiConfig.apiPath}/api/user/AddGuestUser`,
      data: {user:user,guest_user:guest,current_group:current_group}
    }
  )
  .then(response => ({response: response.data}))
  .catch(err => {
    console.log('err')
    console.log(err)
    return ({error: err})
  });
}
function* addGuestSaga(action) {
  console.log('addGuestSaga');

  var user_firebase = yield Firebase.auth().currentUser;
  const user = yield select(getUser);
  const current_group = yield select(getCurrentGroup);
  var idToken = yield user_firebase.getIdToken(false);
  user.idToken = idToken;
  console.log(user);
  var guest =  yield { 
    firebase_uid:'', 
    displayName:action.payload.guest_name, 
    email: '', 
    photoURL: '', 
    idToken: '',
    refreshToken: ''
  };

  let { response, error } = yield call(addMemberApi, action.payload.user, guest, current_group);
  console.log(response);
    if(response)
        yield put({type: "ADD_GUEST_SUCCESS"})
    else
        yield put({type: "ADD_GUEST_FAIL"})
  yield put({type:"GET_GROUP_USERS", payload:{user: user, current_group: current_group}});
  // yield put({type: "GET_ROOMS", payload:{user: user, current_group: action.payload.current_group}})
}
export function* addGuestWatch() {
  yield takeLatest("ADD_GUEST", addGuestSaga);
}
////

