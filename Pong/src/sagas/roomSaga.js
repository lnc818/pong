import { put, takeLatest, all, call, takeEvery, select } from 'redux-saga/effects';
import axios from 'axios';

import { Facebook } from 'expo';

import { config } from '../config';
import { Firebase } from '../integrations/firebase';

import apiConfig from '../config/api'

import AuthService from '../services/Auth';
import NavigationService from '../services/NavigationService'

const getUser = state => state.auth.user;
const getCurrentGroup= state => {if(state.group == null) return null; else return state.group.current_group};
const getCurrentRoom= state => {if(state.room == null) return null; else return state.group.current_room};

///enter room
function* enterRoomSaga(action) {
  console.log('enterRoomSaga');
  yield NavigationService.navigate('Room');
  var user_firebase = yield Firebase.auth().currentUser;

  const user = yield select(getUser);
  var idToken = yield user_firebase.getIdToken(false);
  user.idToken = idToken;
  console.log(user);
  yield put({type: "GET_ROOM_USERS", payload:{user: user, current_room: action.payload.current_room}})
  yield put({type: "GET_HISTORIES", payload:{user: user, current_room: action.payload.current_room}})
}
export function* enterRoomWatch() {
  yield takeLatest("ENTER_ROOM", enterRoomSaga);
}
////
////get room
function getRoomsApi(user, current_group) {
  console.log('getRoomsApi');
  console.log(user, current_group);
  return axios(
    {
      method: 'post',
      url: `${apiConfig.apiPath}/api/room/GetRooms`,
      data: {user:user,current_group: current_group}
    }
  )
  .then(response => ({response: response.data}))
  .catch(err => {
    console.log('err')
    console.log(err)
    return ({error: err})
  });
}

function* getRoomsSaga(action) {
    console.log('getRoomsSaga')
    console.log(action);
    let { response, error } = yield call(getRoomsApi, action.payload.user, action.payload.current_group);
    console.log(response);
    if(response)
        yield put({type: "RECEIVED_ROOMS", rooms: response})
    else
        yield put({type: "GET_ROOMS_FAIL"})
}
export function* getRoomWatch() {
  yield takeLatest("GET_ROOMS", getRoomsSaga);
}
////

////get room users
function getRoomUsersApi(user, current_room) {
  console.log('getRoomUsersApi');
  console.log(user, current_room);
  return axios(
    {
      method: 'post',
      url: `${apiConfig.apiPath}/api/room/GetUsersByRoom`,
      data: {user:user,current_room: current_room}
    }
  )
  .then(response => ({response: response.data}))
  .catch(err => {
    console.log('err')
    console.log(err)
    return ({error: err})
  });
}

function* getRoomUsersSaga(action) {
    console.log('getRoomUsersSaga')
    console.log(action);
    let { response, error } = yield call(getRoomUsersApi, action.payload.user, action.payload.current_room);
    console.log(response);
    if(response)
        yield put({type: "GET_ROOM_USERS_SUCCESS", payload:{current_room_users: response}})
    else
        yield put({type: "GET_ROOM_USERS_FAIL"})
}
export function* getRoomUsersWatch() {
  yield takeLatest("GET_ROOM_USERS", getRoomUsersSaga);
}
////

////add room
function addRoomApi(user, current_group, room) {
  console.log('addRoomApi');
  console.log(user, current_group,room);
  return axios(
    {
      method: 'post',
      url: `${apiConfig.apiPath}/api/room/AddRoom`,
      data: {user:user,current_group:current_group,room:room}
    }
  )
  .then(response => ({response: response.data}))
  .catch(err => {
    console.log('err')
    console.log(err)
    return ({error: err})
  });
}
function* addRoomSaga(action) {
  console.log('addRoomSaga');

  var user_firebase = yield Firebase.auth().currentUser;
  const user = yield select(getUser);
  const current_group = yield select(getCurrentGroup);
  var idToken = yield user_firebase.getIdToken(false);
  user.idToken = idToken;
  console.log(user);

  let { response, error } = yield call(addRoomApi, action.payload.user, current_group, action.payload.room);
  console.log(response);
    if(response)
        yield put({type: "ADD_ROOM_SUCCESS"})
    else
        yield put({type: "ADD_ROOM_FAIL"})

  yield put({type: "GET_ROOMS", payload:{user: user, current_group: current_group}})
}
export function* addRoomWatch() {
  yield takeLatest("ADD_ROOM", addRoomSaga);
}
////