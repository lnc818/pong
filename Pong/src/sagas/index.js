import { put, takeLatest, all, call, fork } from 'redux-saga/effects';
import axios from 'axios';

import { Facebook } from 'expo';

import { config } from '../config';
import { Firebase } from '../integrations/firebase';

import apiConfig from '../config/api'

import AuthService from '../services/Auth';
import NavigationService from '../services/NavigationService'

import * as groupSagas from './groupSaga';
import * as roomSagas from './roomSaga';
import * as historySagas from './historySaga';

function* fetchNews() {
//   const json = yield fetch('https://newsapi.org/v1/articles? 
//         source= cnn&apiKey=c39a26d9c12f48dba2a5c00e35684ecc')
//         .then(response => response.json(), );   

//   yield put({ type: "NEWS_RECEIVED", json: json.articles, });
  yield console.log('fetchNews');
}
function* actionWatcher() {
     yield takeLatest('GET_NEWS', fetchNews)
}

///login
function loginUserApi(user) {
  console.log('loginUserApi');
console.log(apiConfig);
  return axios(
    {
      method: 'post',
      url: `${apiConfig.apiPath}/api/user/LoginUser`,
      data: user
    }
  )
  .then(response => response.data)
  .catch(err => {
    console.log('err')
    console.log(err)
    throw err;
  });
}

function* logInUser() {
  console.log('logInUser')
  const user = yield AuthService.loginWithFacebook();
  console.log('logInUser user');
  console.log(user);
  if(user)
  {
    var user_mapped = { 
                        firebase_uid:user.uid, 
                        displayName:user.displayName, 
                        email: user.email, 
                        photoURL: user.photoURL, 
                        idToken: user.idToken,
                        refreshToken: user.refreshToken
                      };
    let logged_in_user = yield call(loginUserApi, user_mapped);
    console.log('loginUserApi user');
    console.log(logged_in_user);
    yield put({type: "LOGGED_IN_USER", user: logged_in_user})
    yield put({type: "GET_GROUPS", payload:{user: logged_in_user}})
  }
}
function* loginWatcher(){
  yield takeLatest('LOG_IN_USER', logInUser);
}
////

////logout
function* logOutUser() {
  console.log('logOutUser')
  yield AuthService.logout();
  yield put({type: "LOGGED_OUT_USER", user: null})
}
function* logOutWatcher(){
  yield takeLatest('LOG_OUT_USER', logOutUser);
}
////

////group
function addGroupApi(group_name, user) {
  console.log('addGroupApi');
  console.log(group_name);
  var name = 'abc';
  return axios(
    {
      method: 'post',
      url: `${apiConfig.apiPath}/api/group/AddGroup`,
      data: {
          group:{"name": group_name},
          user: user
          // "lastName": this.lastName
      }
    }
  )
  .then(response => response.data)
  .catch(err => {
    console.log('err')
    console.log(err)
    throw err;
  });
}
function* addGroup(action) {
  console.log('addGroup')
  console.log(action.payload.group_name, action.payload.user);
  let  data  = yield call(addGroupApi, action.payload.group_name, action.payload.user);
  console.log(data);
  yield put({type: "ADD_GROUP_SUCCESS", payload:{groups: groups}})
}
function* addGroupWatch(){
  yield takeLatest('ADD_GROUP', addGroup);
}


function getGroupsApi(user) {
  console.log('getGroupsApi');
  console.log(user);
  return axios(
    {
      method: 'post',
      url: `${apiConfig.apiPath}/api/group/GetGroups`,
      data: {user:user}
    }
  )
  .then(response => response.data)
  .catch(err => {
    console.log('err')
    console.log(err)
    throw err;
  });
}
function* getGroups(action) {
  console.log('getGroups')
  console.log(action);
  let groups = yield call(getGroupsApi,action.payload.user);
  console.log(groups);
  yield put({type: "RECEIVED_GROUPS", payload:{groups: groups}})
}
function* getGroupsWatch(){
  yield takeLatest('GET_GROUPS', getGroups);
}
////

////
function* enterGroupSaga(group) {
  console.log('enterGroupSaga');
  yield NavigationService.navigate('Group');
}


export function* enterGroupWatcher() {
yield takeLatest("ENTER_GROUP", enterGroupSaga);
}
////
export default function* rootSaga() {
  console.log('rootSaga');
  [...Object.values(groupSagas)].map((item)=>console.log(item))
   yield all([
   actionWatcher(),
   loginWatcher(),
   logOutWatcher(),
   addGroupWatch(),
   getGroupsWatch(),
   ...[...Object.values(groupSagas)].map(fork),
   ...[...Object.values(roomSagas)].map(fork),
   ...[...Object.values(historySagas)].map(fork),
   ]);
}