export default function groupReducer(state = {groups:[],current_group:null,current_group_users:[]}, action) {
    switch (action.type) {
      case 'ENTER_GROUP':
        return {...state, loading:false, current_group: action.payload.current_group}
      case 'RECEIVED_GROUPS':
        return {...state, loading:false, groups: action.payload.groups};
      case 'GET_GROUPS':
        return {...state, loading: true };
      case 'ADD_GROUP':
        return {...state, loading: true };
      case 'ADD_GROUP_SUCCESS':
        return {...state, loading: false, groups: action.payload.groups };
      case 'ADD_GROUP_FAIL':
        return {...state, loading: false };
      case 'ADD_GUEST':
        return {...state, loading:true};
      case 'ADD_GUEST_SUCCESS':
        return {...state, loading:false};
      case 'ADD_GUEST_FAIL':
        return {...state, loading:false};
      case 'GET_GROUP_USERS':
        return {...state, loading:true};
      case 'GET_GROUP_USERS_SUCCESS':
        return {...state, loading:false, current_group_users: action.payload.current_group_users};
      case 'GET_GROUP_USERS_FAIL':
        return {...state, loading:false, current_group_users: []};
      default:
        return state
    }
  }