export default function historyReducer(state = {histories:[]}, action) {
    switch (action.type) {
      case 'ADD_HISTORY':
        return {...state, loading:true};
      case 'ADD_HISTORY_SUCCESS':
        return {...state, loading:false};
      case 'ADD_HISTORY_FAIL':
        return {...state, loading:false};
      case 'GET_HISTORIES':
        return {...state, loading:true};
      case 'GET_HISTORIES_SUCCESS':
        return {...state, loading:false, histories: action.payload.histories};
      case 'GET_HISTORIES_FAIL':
        return {...state, loading:false, histories: []};
      default:
        return state
    }
  }