export default function roomReducer(state = {rooms:[],current_room:null,current_room_users:[]}, action) {
    switch (action.type) {
      case 'ENTER_ROOM':
        return {...state, loading:false, current_room: action.payload.current_room}
      case 'RECEIVED_ROOMS':
        return {...state, loading:false, rooms: action.rooms};
      case 'GET_ROOMS':
        return {...state, loading: true };
      case 'GET_ROOMS_FAIL':
        return {...state, loading: false };
      case 'GET_ROOM_USERS':
        return {...state, loading: true };
      case 'GET_ROOM_USERS_SUCCESS':
        return {...state, loading: false, current_room_users: action.payload.current_room_users};
      case 'GET_ROOM_USERS_FAIL':
        return {...state, loading: false };
      case 'ADD_ROOM':
        return {...state, loading: true };
      case 'ADD_ROOM_SUCCESS':
        return {...state, loading: false };
      case 'ADD_ROOM_FAIL':
        return {...state, loading: false };
      default:
        return state
    }
  }