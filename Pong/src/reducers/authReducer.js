export default function authReducer(state = {loading:false,user:null}, action) {
    switch (action.type) {
      case 'LOG_IN_USER':
      {
        console.log('LOG_IN_USER');
        return { ...state, loading: true };
      }
      case 'LOGGED_IN_USER':
        return { ...state, loading: false, user: action.user };
      case 'LOG_OUT_USER':
      {
        console.log('LOG_OUT_USER');
        return { ...state, loading: true };
      }
      case 'LOGGED_OUT_USER':
      return { ...state, loading: false, user: null };
      default:
        return state
    }
  }