import { combineReducers } from 'redux'
import authReducer from './authReducer'
import groupReducer from './groupReducer'
import roomReducer from './roomReducer'
import historyReducer from './historyReducer'

export default combineReducers({
  auth: authReducer,
  group: groupReducer,
  room: roomReducer,
  history: historyReducer
})