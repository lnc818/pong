import React from 'react';
import { StyleSheet, Text, View, Button, Image } from 'react-native';
import AuthService from '../services/Auth';

import { connect } from "react-redux";

import { config } from '../config'; 
import apiPath from '../config/api'

import { log_in_user, logged_in_user, log_out_user } from '../actions'

// const mapStateToProps = state => {
//     // return { user: state.user };
//     return {};
//   };


const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });

 class AuthScreen extends React.Component {

      constructor(props) {
        super(props);
        this.state = {
            loading:false,
            user: null
        };
      }
      componentDidMount() {
        var self = this;
        console.log('AuthScreen mounted');
        // AuthService.subscribeAuthChange(user => {
        //     console.log('subscribeAuthChange');
        //     console.log(user);
        //     // self.setState({ user: user }, console.log(self.state))
        //     // if(user)
        //     // this.props.log_in_user(user);
        //     // this.props.logged_in_user(user);
        // });
      }

      render() {
        if (this.props.user) {
            const avatar = this.props.user.photoURL && (
              <Image style={{ width: 50, height: 50 }} source={{ uri: this.props.user.photoURL }} />
            );
            return (
              <View style={styles.container}>
                <Text>You are logged in!</Text>
                {avatar}
                {/* <Button onPress={AuthService.logout} title='Logout' /> */}
                <Button onPress={this.props.log_out_user} title='Logout' />
                
              </View>
            );
            
          }
      
          return (
            <View style={styles.container}>
              <Text>Welcome! 20190712V5</Text>
              {/* <Button onPress={AuthService.loginWithFacebook} title='Login with Facebook' /> */}
              <Button onPress={this.props.log_in_user} title='Login with Facebook' />
            </View>
          );
      }
    }

    const mapStateToProps = state => ({
        loading: state.auth.loading,
        user: state.auth.user
      });
    const mapDispatchToProps = {
        log_in_user: log_in_user,
        logged_in_user: logged_in_user,
        log_out_user: log_out_user
   };

    export default connect(mapStateToProps,mapDispatchToProps)(AuthScreen);