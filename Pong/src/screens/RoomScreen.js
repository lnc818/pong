import React from 'react';
import { Text, Platform, View, StyleSheet, ScrollView } from 'react-native';
import { Button, Paragraph, Dialog, Portal, FAB, List, Appbar, Avatar, Card, Title, DataTable   } from 'react-native-paper';
import FoldView from 'react-native-foldview';

import { connect } from "react-redux";

import Dimensions from 'Dimensions';

import AddHistoryDialog from '../components/room/AddHistoryDialog'
import Row from '../components/foldview/Row';

import NavigationService from '../services/NavigationService'
import PlayerRow from '../components/room/PlayerRow/PlayerRow';


let ScreenHeight = Dimensions.get("window").height;
const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : 0;
const styles = StyleSheet.create({
  fab: {
    position: 'absolute',
    margin: 16,
    right: 50,
    top: ScreenHeight - 150,
  },
  fab2: {
    position: 'absolute',
    margin: 16,
    right: 0,
    top: ScreenHeight - 150,
  },
  container: {
    flex: 1,
    height: '200%'
  },
  scrollView: {
    backgroundColor: '#4A637D',
    flex: 1,
    padding: 10,
    paddingTop: STATUSBAR_HEIGHT,
    height: '200%'
  },
})
class RoomScreen extends React.Component {

  static navigationOptions = {
    header: null
  }


  constructor(props){


    super(props);
    this.state = {
     visible: false,
     fabOpen:true,
     expanded: false,
    };
  }
  _showDialog = () => {this.setState({ visible: true })};

  _hideDialog = () => {this.setState({ visible: false })};

  flip = () => {
    this.setState({
      expanded: !this.state.expanded,
    });
  }
  renderPlayerRows = () => {
    var self = this;
    let histories = this.props.histories;
    let current_room = this.props.room.current_room;
    let current_room_users = this.props.room.current_room_users;
    if(current_room == null || current_room_users == null || current_room_users.length < 4)
      return null;
    console.log('renderPlayerRows');
    console.log(current_room_users);
    let { u1_sum, u2_sum, u3_sum, u4_sum } = histories.reduce((sum, item)=>{
        return { 
          u1_sum: sum.u1_sum + item.amount_u1,
          u2_sum: sum.u2_sum + item.amount_u2,
          u3_sum: sum.u3_sum + item.amount_u3,
          u4_sum: sum.u4_sum + item.amount_u4,
        }
      },
      {u1_sum:0,u2_sum:0,u3_sum:0,u4_sum:0}
    );
    
    let temp_sum = {amount_u1:0,amount_u2:0,amount_u3:0,amount_u4:0}
    let histories_acc = histories.reduce((sum, item)=>{
      item = {
              ...item, 
              amount_u1: item.amount_u1 + temp_sum.amount_u1,
              amount_u2: item.amount_u2 + temp_sum.amount_u2,
              amount_u3: item.amount_u3 + temp_sum.amount_u3,
              amount_u4: item.amount_u4 + temp_sum.amount_u4
            }
            sum.push(item);
	    temp_sum = {amount_u1:item.amount_u1,amount_u2:item.amount_u2,amount_u3:item.amount_u3,amount_u4:item.amount_u4};
      return sum;
    },[]);

    let highest_ind = {
      amount_u1:Math.max(...histories.map((item)=>{return item.amount_u1})),
      amount_u2:Math.max(...histories.map((item)=>{return item.amount_u2})),
      amount_u3:Math.max(...histories.map((item)=>{return item.amount_u3})),
      amount_u4:Math.max(...histories.map((item)=>{return item.amount_u4})),
    }

    let lowest_ind = {
      amount_u1:Math.min(...histories.map((item)=>{return item.amount_u1})),
      amount_u2:Math.min(...histories.map((item)=>{return item.amount_u2})),
      amount_u3:Math.min(...histories.map((item)=>{return item.amount_u3})),
      amount_u4:Math.min(...histories.map((item)=>{return item.amount_u4})),
    }

    let highest = {
      amount_u1:Math.max(...histories_acc.map((item)=>{return item.amount_u1})),
      amount_u2:Math.max(...histories_acc.map((item)=>{return item.amount_u2})),
      amount_u3:Math.max(...histories_acc.map((item)=>{return item.amount_u3})),
      amount_u4:Math.max(...histories_acc.map((item)=>{return item.amount_u4})),
    }

    let lowest = {
      amount_u1:Math.min(...histories_acc.map((item)=>{return item.amount_u1})),
      amount_u2:Math.min(...histories_acc.map((item)=>{return item.amount_u2})),
      amount_u3:Math.min(...histories_acc.map((item)=>{return item.amount_u3})),
      amount_u4:Math.min(...histories_acc.map((item)=>{return item.amount_u4})),
    }

    let latest_history_acc = histories_acc.length == 0 ? {amount_u1:0,amount_u2:0,amount_u3:0,amount_u4:0} : histories_acc.slice(-1)[0];
    let mean = {
      amount_u1:latest_history_acc == null ? 0 : latest_history_acc.amount_u1/histories_acc.length,
      amount_u2:latest_history_acc == null ? 0 : latest_history_acc.amount_u2/histories_acc.length,
      amount_u3:latest_history_acc == null ? 0 : latest_history_acc.amount_u3/histories_acc.length,
      amount_u4:latest_history_acc == null ? 0 : latest_history_acc.amount_u4/histories_acc.length,
      // amount_u1: Math.round(latest_history_acc.amount_u1 * 100) / 100,
      // amount_u2: Math.round(latest_history_acc.amount_u2 * 100) / 100,
      // amount_u3: Math.round(latest_history_acc.amount_u3 * 100) / 100,
      // amount_u4: Math.round(latest_history_acc.amount_u4 * 100) / 100,
    }

    let player_histories = {
      u1:histories_acc.length==0 ? [] : histories_acc.map((item,index)=>{return {...item, id:item.uid, y:amount_u1, x:index}}),
      u2:histories_acc.length==0 ? [] : histories_acc.map((item,index)=>{return {...item, id:item.uid, y:amount_u2, x:index}}),
      u3:histories_acc.length==0 ? [] : histories_acc.map((item,index)=>{return {...item, id:item.uid, y:amount_u3, x:index}}),
      u4:histories_acc.length==0 ? [] : histories_acc.map((item,index)=>{return {...item, id:item.uid, y:amount_u4, x:index}})
    }

    let player_details = [
      {highest:highest.amount_u1,lowest:lowest.amount_u1,mean:mean.amount_u1,highest_ind:highest_ind.amount_u1,lowest_ind:lowest_ind.amount_u1,amount_acc: histories_acc.amount_u1},
      {highest:highest.amount_u2,lowest:lowest.amount_u2,mean:mean.amount_u2,highest_ind:highest_ind.amount_u2,lowest_ind:lowest_ind.amount_u2,amount_acc: histories_acc.amount_u2},
      {highest:highest.amount_u3,lowest:lowest.amount_u3,mean:mean.amount_u3,highest_ind:highest_ind.amount_u3,lowest_ind:lowest_ind.amount_u3,amount_acc: histories_acc.amount_u3},
      {highest:highest.amount_u4,lowest:lowest.amount_u4,mean:mean.amount_u4,highest_ind:highest_ind.amount_u4,lowest_ind:lowest_ind.amount_u4,amount_acc: histories_acc.amount_u4},
    ]

    let u_sum = [
      {row_id: 1, player_uid: current_room.u1, sum: u1_sum, player:current_room_users[0], player_details:player_details[0], player_current_score:latest_history_acc.amount_u1, player_histories: player_histories.u1},
      {row_id: 2, player_uid: current_room.u2, sum: u2_sum, player:current_room_users[1], player_details:player_details[1], player_current_score:latest_history_acc.amount_u2, player_histories: player_histories.u2},
      {row_id: 3, player_uid: current_room.u3, sum: u3_sum, player:current_room_users[2], player_details:player_details[2], player_current_score:latest_history_acc.amount_u3, player_histories: player_histories.u3},
      {row_id: 4, player_uid: current_room.u4, sum: u4_sum, player:current_room_users[3], player_details:player_details[3], player_current_score:latest_history_acc.amount_u4, player_histories: player_histories.u4},
    ].sort((a, b)=>{return b.sum-a.sum});
    let zIndex = 100;
    console.log('u_sum');
    console.log(u_sum);
    return u_sum.map((item, index)=>{
      return <PlayerRow 
                id={`PlayerRow-${item.row_id}`} 
                zIndex={zIndex - index * 10} 
                player_number={item.row_id}
                player_uid={item.player_uid} 
                player={item.player} 
                player_details={item.player_details}
                player_current_score={item.player_current_score}
                player_histories={item.player_histories}
              />
    })
    return null
  }
  render() {
    console.log('render.props');
    console.log(this.props.navigation);
    return (        
        <View style={styles.container}>
          <Appbar.Header style={{backgroundColor:'#876534'}}>
            {/* <Appbar.BackAction
            /> */}
            <Appbar.Content
              title="Rooms"
              subtitle="below show all the rooms in this group"
            />
            <Appbar.Action icon="search"  />
            <Appbar.Action icon="more-vert"/>
          </Appbar.Header>
          <ScrollView
            style={styles.scrollView}
          >
            {/* <Row zIndex={100} />
            <Row zIndex={90} />
            <Row zIndex={80} />
            <Row zIndex={70} /> */}
            {
              this.renderPlayerRows()
            }
          </ScrollView>
          <FAB
            style={styles.fab}
            small
            icon="add"
            onPress={() => this.setClickAddHistoryDialogChild()}
          />
          <AddHistoryDialog 
            setClickAddHistoryDialog={click => this.setClickAddHistoryDialogChild = click}
          />
        </View>
        
        // <FAB
        //     style={styles.fab}
        //     small
        //     icon="casino"
        //     onPress={() => this.setClickAddHistoryDialogChild()}
        //   />
        // <AddHistoryDialog 
        //   setClickAddHistoryDialog={click => this.setClickAddHistoryDialogChild = click}
        // />
      // </View>
    );
  }
}

  const mapStateToProps = state => ({
    loading: state.auth.loading,
    user: state.auth.user,
    groups: state.group.groups,
    room: state.room,
    histories: state.history.histories
  });
  const mapDispatchToProps = {
      // add_group: add_group,
  };
  export default connect(mapStateToProps,mapDispatchToProps)(RoomScreen);