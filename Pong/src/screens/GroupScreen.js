import React from 'react';
import { Text, View, StyleSheet  } from 'react-native';
import { Button, Paragraph, Dialog, Portal, FAB, List, Appbar   } from 'react-native-paper';

import { connect } from "react-redux";

import Dimensions from 'Dimensions';

import AddRoomDialog from '../components/room/AddRoomDialog'
import AddPlayerDialog from '../components/room/AddPlayerDialog'

import NavigationService from '../services/NavigationService'

import { enter_room } from '../actions'

let ScreenHeight = Dimensions.get("window").height;
const styles = StyleSheet.create({
  fab: {
    position: 'absolute',
    margin: 16,
    right: 50,
    top: ScreenHeight - 150,
  },
  fab2: {
    position: 'absolute',
    margin: 16,
    right: 0,
    top: ScreenHeight - 150,
  },
})
class GroupScreen extends React.Component {

  static navigationOptions = {
    header: null
  }


  constructor(props){


    super(props);
    this.state = {
     visible: false,
     fabOpen:true
    };
  }
  enter_room = (room) =>{
    this.props.enter_room(room);
  }

  _showDialog = () => {this.setState({ visible: true })};

  _hideDialog = () => {this.setState({ visible: false })};
      render() {
        var self = this;
        console.log('render.props');
        console.log(this.props.navigation);
        return (
          <View >
            <Appbar.Header style={{backgroundColor:'#456534'}}>
              {/* <Appbar.BackAction
              /> */}
              <Appbar.Content
                title="Rooms"
                subtitle="below show all the rooms in this group"
              />
              <Appbar.Action icon="search"  />
              <Appbar.Action icon="more-vert"/>
            </Appbar.Header>
            <List.Section>
                 {/* <List.Subheader>Some title</List.Subheader> */}
                 {
                   this.props.rooms.map((room)=>{
                      return (
                        <List.Item
                          key={`room-${room.uid}`}
                          title={room.name}
                          left={() => <List.Icon icon="group" />}
                          right={() => <List.Icon icon="keyboard-arrow-right" />}
                          onPress={() => 
                            self.props.enter_room(room)
                            // this.props.navigation.navigate('Room')
                          }
                        />
                      )
                   })
                 }
            </List.Section>
            
            <FAB
                style={styles.fab}
                small
                icon="casino"
                onPress={() => this.setClickAddRoomDialogChild()}
              />
            <FAB
              style={styles.fab2}
              small
              icon="people"
              onPress={() => this.setClickAddMemberDialogChild()}
            />
            <AddRoomDialog 
              setClickAddRoomDialog={click => this.setClickAddRoomDialogChild = click}
            />
            <AddPlayerDialog 
              setClickAddMemberDialog={click => this.setClickAddMemberDialogChild = click}
            />
              {/* <Portal>
            <FAB.Group
                style={styles.fab2}
                open={this.state.fabOpen}
                icon={this.state.fabOpen ? 'today' : 'add'}
                actions={[
                  { icon: 'casino', label: 'room', onPress:() => this.setClickAddRoomDialogChild()},
                  { icon: 'people', label: 'member', onPress:() => this.setClickAddMemberDialogChild()},

                ]}
                onStateChange={({ fabOpen }) => this.setState({ fabOpen })}
                onPress={(fabOpen) => {
                  this.setState({ fabOpen })
                  if (this.state.fabOpen) {
                    // do something if the speed dial is open
                  }
                }}
              />
              
              </Portal> */}
          </View>
        );
      }
    }

  const mapStateToProps = state => ({
    loading: state.auth.loading,
    user: state.auth.user,
    groups: state.group.groups,
    rooms: state.room.rooms
  });
  const mapDispatchToProps = {
    enter_room: enter_room,
  };
    export default connect(mapStateToProps,mapDispatchToProps)(GroupScreen);