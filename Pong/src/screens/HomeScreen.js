import React from 'react';
import { Text, View, StyleSheet  } from 'react-native';
import { Button, Paragraph, Dialog, Portal, FAB, List, Appbar   } from 'react-native-paper';

import { connect } from "react-redux";

import Dimensions from 'Dimensions';

import AddGroupDialog from '../components/group/AddGroupDialog'

import NavigationService from '../services/NavigationService'

import { enter_group, log_in_user, logged_in_user, log_out_user } from '../actions'

let ScreenHeight = Dimensions.get("window").height;
const styles = StyleSheet.create({
  fab: {
    position: 'absolute',
    margin: 16,
    right: 0,
    top: ScreenHeight - 150,
  },
})
class HomeScreen extends React.Component {

  static navigationOptions = {
    header: null
  }


  constructor(props){


    super(props);
    this.state = {
     visible: false,
    };
  }
  _showDialog = () => {this.setState({ visible: true })};

  _hideDialog = () => {this.setState({ visible: false })};

  _enterGroup = (group) =>{
    this.props.enter_group(group);
  }
      render() {
        var self = this;
        return (
          <View >
            <Appbar.Header>
              {/* <Appbar.BackAction
              /> */}
              <Appbar.Content
                title="Groups"
                subtitle="below show all your groups"
              />
              <Appbar.Action icon="search"  />
              <Appbar.Action icon="more-vert"/>
            </Appbar.Header>
            <List.Section>
                 {/* <List.Subheader>Some title</List.Subheader> */}
                 {
                   this.props.groups.map((group)=>{
                      return (
                        <List.Item
                          key={`group-${group.uid}`}
                          title={group.name}
                          left={() => <List.Icon icon="group" />}
                          right={() => <List.Icon icon="keyboard-arrow-right" />}
                          onPress={() => 
                          self.props.enter_group(group)
                           // NavigationService.navigate('Group')
                           // this.props.navigation.navigate('Group')
                          }
                        />
                      )
                   })
                 }
            </List.Section>
            <FAB
                style={styles.fab}
                small
                icon="add"
                onPress={() => this.setClickAddGroupDialogChild()}
              />
            <AddGroupDialog setClickAddGroupDialog={click => this.setClickAddGroupDialogChild = click}/>
            {/* <View style={{ flex: 3, justifyContent: 'center', alignItems: 'center' }}>

              <Text>Home!</Text>
              <Button
                // title="Go to Settings"
                onPress={() => this.props.navigation.navigate('Settings')}
              >Go to Settings
              </Button>
              <Button
                // title="Go to Details"
                onPress={() => this.props.navigation.navigate('Details')}
              >
              Go to Details
              </Button>


              <AddGroupDialog />
            </View> */}
          </View>
        );
      }
    }

  const mapStateToProps = state => ({
    loading: state.auth.loading,
    user: state.auth.user,
    groups: state.group.groups
  });
  const mapDispatchToProps = {
    enter_group: enter_group,

  };
    export default connect(mapStateToProps,mapDispatchToProps)(HomeScreen);