export const getNews = () => ({
    type: 'GET_NEWS',
});
export const log_in_user = () => ({
    type: 'LOG_IN_USER',    
});
export const log_out_user = () => ({
    type: 'LOG_OUT_USER',    
});

////group
export const enter_group = (current_group) => ({
    type: 'ENTER_GROUP',
    payload: {current_group: current_group}
})
export const get_groups = (user) => ({
    type: 'GET_GROUPS',
    payload: {user: user}
})
export const add_group = (group_name,user) => ({
    type: 'ADD_GROUP',
    payload: {group_name: group_name, user: user}
})
export const add_guest = (user,guest_name) => ({
    type: 'ADD_GUEST',
    payload: {user: user, guest_name: guest_name}
})
////

////room
export const enter_room = (current_room) => ({
    type: 'ENTER_ROOM',
    payload: {current_room: current_room}
})
export const add_room = (user, current_room, history) => ({
    type: 'ADD_ROOM',
    payload: {user: user, current_room: current_room, history: history}
})

///

////history
export const enter_history = (current_room) => ({
    type: 'ENTER_HISTORY',
    payload: {current_room: current_room}
})
export const add_history = (user, current_room) => ({
    type: 'ADD_HISTORY',
    payload: {user: user, current_room: current_room}
}) 
////