import * as firebase from 'firebase';

import { config } from '../config';

// console.log('initializeApp start')
 firebase.initializeApp(config.firebase);
// console.log('initializeApp end')

export const Firebase = firebase;
