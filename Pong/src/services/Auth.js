import { Facebook } from 'expo';

import { config } from '../config';
import { Firebase } from '../integrations/firebase';

import { connect } from "react-redux";

import { logged_in } from '../actions'


 class AuthService {
  /**
   * Login with Facebook and Firebase
   *
   * Uses Expo Facebook API and authenticates the Facebook user in Firebase
   */
  static async loginWithFacebook() {
    // console.log('logInWithReadPermissionsAsync start');
    const { type, token } = await Facebook.logInWithReadPermissionsAsync(
      config.facebook.appId,
      { permissions: ['public_profile', 'email'] },
    );
    // console.log('logInWithReadPermissionsAsync end');

    if (type === 'success' && token) {
      // Build Firebase credential with the Facebook access token.

      // console.log('Firebase.auth.FacebookAuthProvider.credential start');
      const credential = Firebase.auth.FacebookAuthProvider.credential(token);
      // console.log('Firebase.auth.FacebookAuthProvider.credential end');

      // Sign in with credential from the Facebook user.
      // console.log('signInAndRetrieveDataWithCredential start');
      var {user} = await Firebase
        .auth()
        .signInAndRetrieveDataWithCredential(credential);
      // console.log('signInAndRetrieveDataWithCredential end');

      var idToken = await user.getIdToken(false);

      var user_mapped = {displayName:user.displayName,email:user.email,photoURL:user.photoURL, idToken:idToken, refreshToken: user.refreshToken}
      // var user_mapped = {displayName:'abc',email:'edf',photoURL:'fff'}
      console.log(user_mapped);
      return {...user,idToken: idToken};
    }
    else return null
  }

  /**
   * Register a subscription callback for changes of the currently authenticated user
   * 
   * @param callback Called with the current authenticated user as first argument
   */
  static subscribeAuthChange(callback) {
    Firebase.auth().onAuthStateChanged(callback);
  }

  static async logout() {
    return Firebase.auth().signOut();
  }
}
const mapDispatchToProps = {
  logged_in: logged_in,
};

export default connect(null,mapDispatchToProps)(AuthService);
