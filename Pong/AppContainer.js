import App from './src/App';

import React from 'react';
import { Button, Text, View } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { createStackNavigator, createBottomTabNavigator, createAppContainer } from 'react-navigation';

// import { render } from 'react-dom';
import { connect } from "react-redux";
////


import HomeScreen from './src/screens/HomeScreen';
import GroupScreen from './src/screens/GroupScreen';
import RoomScreen from './src/screens/RoomScreen';
import AuthScreen from './src/screens/AuthScreen';
import foldview from './src/components/foldview/foldeview';

import NavigationService from './src/services/NavigationService'

// class HomeScreen extends React.Component {
//   render() {
//     return (
//       <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
//         <Text>Home!</Text>
//         <Button
//           title="Go to Settings"
//           onPress={() => this.props.navigation.navigate('Settings')}
//         />
//         <Button
//           title="Go to Details"
//           onPress={() => this.props.navigation.navigate('Details')}
//         />
//       </View>
//     );
//   }
// }

class SettingsScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>Settings!!</Text>
        <Button
          title="Go to Home"
          onPress={() => this.props.navigation.navigate('Home')}
        />
        <Button
          title="Go to Details"
          onPress={() => this.props.navigation.navigate('Details')}
        />
      </View>
    );
  }
}

class DetailsScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <App/>
      </View>
    );
  }
}

const HomeStack = createStackNavigator({
  Home: { screen: HomeScreen },
  Group: { screen:GroupScreen },
  Room: {screen: RoomScreen},
  foldview: {screen: foldview},
  Details: { screen: DetailsScreen },
});

const SettingsStack = createStackNavigator({
  Settings: { screen: SettingsScreen },
  Details: { screen: DetailsScreen },
});

var NavigationStack = createAppContainer(createBottomTabNavigator(
  {
    Home: { screen: HomeStack },
    Settings: { screen: SettingsStack },
    Auth: {screen: AuthScreen}
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state;
        let iconName;
        if (routeName === 'Home') {
          iconName = `ios-information-circle${focused ? '' : '-outline'}`;
        } else if (routeName === 'Settings') {
          iconName = `ios-options${focused ? '' : '-outline'}`;
        }

        // You can return any component that you like here! We usually use an
        // icon component from react-native-vector-icons
        return <Ionicons name={iconName} size={25} color={tintColor} />;
        // return <Text>abc</Text>
      },
    }),
    tabBarOptions: {
      activeTintColor: 'tomato',
      inactiveTintColor: 'gray',
    },
  }
));

class AppContainer extends React.Component {
  componentDidUpdate(){
  }
  render() {
      console.log('this.props');
      console.log(this.props);
      if(this.props.user == null)
      return (
        <AuthScreen />
      )
      else
      return (
        <NavigationStack                 
        ref={navigatorRef => {
          NavigationService.setTopLevelNavigator(navigatorRef);
        }}
        />
          // <View>
            
          //   {/* {this.state.auth == null ? <NavigationStack /> : <AuthScreen />} */}
          // </View>
          // <Provider store={store}>
          //     <PersistGate persistor={persistor}>
          //         <PaperProvider theme={theme}>
          //             
          //         </PaperProvider>
          //     </PersistGate>
          // </Provider>
      );
  }
}
// export default App;
const mapStateToProps = state => {
  return {
    loading: state.auth.loading,
    user: state.auth.user
  };
};

// export default connect(null,null)(Entrypoint);
export default connect(mapStateToProps,null)(AppContainer);
