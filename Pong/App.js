import App from './src/App';

import React from 'react';
import { Button, Text, View } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { createStackNavigator, createBottomTabNavigator, createAppContainer } from 'react-navigation';

////redux and saga
import createSagaMiddleware from 'redux-saga';
// import { render } from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { logger } from 'redux-logger';
import reducer from './src/reducers';
import rootSaga from './src/sagas';

import { connect } from "react-redux";
////


import HomeScreen from './src/screens/HomeScreen';
import AuthScreen from './src/screens/AuthScreen';
import AppContainer from './AppContainer';

import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';

import NavigationService from './src/services/NavigationService';

const sagaMiddleware = createSagaMiddleware();

const store = createStore(
   reducer,
   applyMiddleware(sagaMiddleware, logger),
);
sagaMiddleware.run(rootSaga);

const theme = {
  ...DefaultTheme,
  roundness: 2,
  colors: {
    ...DefaultTheme.colors,
    primary: '#3498db',
    accent: '#f1c40f',
  }
};

class Entrypoint extends React.Component {
  componentDidUpdate(){
    console.log('Entrypoint Update');
    var state = store.getState();
    console.log(state);
  }
  render() {
      console.log('Entrypoint rendered');
      var state = store.getState();
      console.log(state);
      return (
          <Provider store={store}>
            {/* {state.auth == null ? <NavigationStack /> : <AuthScreen />} */}
            {/* <NavigationStack /> */}
            <PaperProvider theme={theme}>
              <AppContainer 
              />
            </PaperProvider>
          </Provider>
          // <Provider store={store}>
          //     <PersistGate persistor={persistor}>
          //         <PaperProvider theme={theme}>
          //             
          //         </PaperProvider>
          //     </PersistGate>
          // </Provider>
      );
  }
}
export default Entrypoint;
