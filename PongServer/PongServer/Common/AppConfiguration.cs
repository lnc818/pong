﻿using FirebaseAdmin;
using Google.Apis.Auth.OAuth2;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace PongServer.Common
{
    public class FireBaseConfiguration
    {
        private readonly string _firebasePath = string.Empty;
        private FirebaseApp _firebaseApp = null;
        private FireBaseConfiguration()
        {
            var configurationBuilder = new ConfigurationBuilder();
            var path = Path.Combine(Directory.GetCurrentDirectory(), "pongappauth-firebase-adminsdk-o0665-1aaddf7c7b.json");

            _firebasePath = path;

            _firebaseApp = FirebaseApp.Create(new AppOptions()
            {
                Credential = GoogleCredential.FromFile(_firebasePath),
            });
        }
        public string FireBasePath
        {
            get => _firebasePath;
        }
        public FirebaseApp FirebaseApp
        {
            get => _firebaseApp;
        }



        private static Lazy<FireBaseConfiguration> instance = new Lazy<FireBaseConfiguration>(() => new FireBaseConfiguration());
        private object fireBaseConfiguration;

        public static FireBaseConfiguration Instance => instance.Value;
    }
}
