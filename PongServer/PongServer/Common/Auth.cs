﻿using FirebaseAdmin;
using FirebaseAdmin.Auth;
using PongServer.DAL;
using PongServer.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PongServer.Common
{
    public class Auth
    {
        public static async Task<UserDTO> loginUser(string token)
        {
            ResponseDTO responseDTO = new ResponseDTO();
            UserDTO userDTO = await GetUserByToken(token);
            if (userDTO == null)
            {
                //throw new Exception("Login Failed");
                return null;
            }
                

            if (userDTO.idToken != token)
            {
                userDTO.idToken = token;
                userDTO.last_login_date = DateTime.Now;
                UserDAL.updateUser(userDTO);
            }
            return userDTO;

        }
        public static async Task<UserDTO> GetUserByToken(string token)
        {
            ResponseDTO responseDTO = new ResponseDTO();
            FireBaseConfiguration fireBaseConfiguration = FireBaseConfiguration.Instance;

            FirebaseToken decodedToken;
            try
            {
                decodedToken = await FirebaseAuth.DefaultInstance
                                            .VerifyIdTokenAsync(token);
                string firebase_uid = decodedToken.Uid;

                UserDTO userDTO = new UserDTO();
                userDTO = UserDAL.getUser(firebase_uid);

                if (userDTO == null)
                    return null;
                if (userDTO.idToken != token)
                {
                    userDTO.idToken = token;
                    userDTO.last_login_date = DateTime.Now;
                    UserDAL.updateUser(userDTO);
                }
                return userDTO;
            }
            catch(ArgumentException ex)
            {
                responseDTO = new ResponseDTO(false, ERROR_TYPE.ARGUMENT_ERROR, "ArgumentException: " + ex.Message);
                return null;
            }
            catch(FirebaseException ex)
            {
                responseDTO = new ResponseDTO(false, ERROR_TYPE.EXPIRED, "FirebaseException: " + ex.Message);
                return null;
            } 
        }

        public static async Task<bool> checkAuthUser(UserDTO userDTO)
        {
            UserDTO userDTO_by_token = await GetUserByToken(userDTO.idToken);
            if (userDTO == null)
                return false;
            else
                return true;
        }
    }
}
