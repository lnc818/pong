﻿using PongServer.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PongServer.BL
{
    public class RoomBL
    {
        public bool validateRoom(RoomDTO roomDTO)
        {
            try
            {
                if (roomDTO.group_uid <= 0)
                {
                    throw new Exception("Missing group uid");
                }
                if (roomDTO.name.Trim() == "")
                {
                    throw new Exception("Missing room name");
                }
                if (roomDTO.max <= 0 || roomDTO.max > 13)
                {
                    throw new Exception("Invalid min value");
                }
                if (roomDTO.max <= 0 || roomDTO.max > 13)
                {
                    throw new Exception("Invalid max value");
                }
                if (roomDTO.min > roomDTO.max)
                {
                    throw new Exception("Min Must smaller than max");
                }
               
                if (roomDTO.u1 <= 0)
                {
                    throw new Exception("Missing player 1");
                }
                if (roomDTO.u2 <= 0)
                {
                    throw new Exception("Missing player 2");
                }
                if (roomDTO.u3 <= 0)
                {
                    throw new Exception("Missing player 3");
                }
                if (roomDTO.u4 <= 0)
                {
                    throw new Exception("Missing player 4");
                }
                List<int> players = new List<int>();
                players.Add(roomDTO.u1);
                if(players.Exists(x=>x==roomDTO.u2))
                {
                    throw new Exception("Duplicate player");
                }
                players.Add(roomDTO.u2);
                if (players.Exists(x => x == roomDTO.u3))
                {
                    throw new Exception("Duplicate player");
                }
                players.Add(roomDTO.u3);
                if (players.Exists(x => x == roomDTO.u4))
                {
                    throw new Exception("Duplicate player");
                }
                players.Add(roomDTO.u4);

                
                foreach(string item in roomDTO.size.Split(";"))
                {
                    roomDTO.size_list.Add(Convert.ToDecimal(item));
                }

                if (roomDTO.size_list.Count != 13)
                {
                    throw new Exception("Missing size");
                }

                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }
    }
}
