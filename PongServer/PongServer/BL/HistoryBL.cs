﻿using PongServer.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PongServer.BL
{
    public static class HistoryBL
    {
        public static bool validateHistory(HistoryDTO historyDTO, RoomDTO roomDTO)
        {
            try
            {
                List<int> room_users_uid = new List<int>();
                room_users_uid.Add(roomDTO.u1);
                room_users_uid.Add(roomDTO.u2);
                room_users_uid.Add(roomDTO.u3);
                room_users_uid.Add(roomDTO.u4);
                if (historyDTO.room_uid <= 0)
                {
                    throw new Exception("Missing room uid");
                }
                if (historyDTO.size <= 0 || historyDTO.size > 13)
                {
                    throw new Exception("Invalid size");
                }
                if (!room_users_uid.Exists(x => x == historyDTO.winner))
                {
                    throw new Exception("Invalid winner uid");
                }
                if (historyDTO.loser!= 0 && !room_users_uid.Exists(x => x == historyDTO.loser))
                {
                    throw new Exception("Invalid loser uid");
                }

                if (historyDTO.is_one_lose_all == null)
                {
                    throw new Exception("Missing is one lose all");
                }
                if (historyDTO.is_one_lose_all == true && historyDTO.loser == 0)
                {
                    throw new Exception("Invalid is one lose all");
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static HistoryDTO CalHistoryFromUserInput(HistoryDTO historyDTO, RoomDTO roomDTO, UserDTO userDTO_authed)
        {
            ////Update history info
            decimal size_base = roomDTO.size_list[historyDTO.size - 1];
            if (historyDTO.winner == roomDTO.u1)
            {
                if (historyDTO.is_one_lose_all)
                    historyDTO.amount_u1 = size_base * (decimal)1.50;
                else
                    historyDTO.amount_u1 = size_base;
            }
            else if (historyDTO.winner == roomDTO.u2)
            {
                if (historyDTO.is_one_lose_all)
                    historyDTO.amount_u2 = size_base * (decimal)1.50;
                else
                    historyDTO.amount_u2 = size_base;
            }
            else if (historyDTO.winner == roomDTO.u3)
            {
                if (historyDTO.is_one_lose_all)
                    historyDTO.amount_u3 = size_base * (decimal)1.50;
                else
                    historyDTO.amount_u3 = size_base;
            }
            else if (historyDTO.winner == roomDTO.u4)
            {
                if (historyDTO.is_one_lose_all)
                    historyDTO.amount_u4 = size_base * (decimal)1.50;
                else
                    historyDTO.amount_u4 = size_base;
            }


            if (historyDTO.loser == 0)
            {
                if (historyDTO.is_one_lose_all)
                {
                    throw new Exception("Missing loser.");
                }
                else
                {
                    if (historyDTO.amount_u1 == 0)
                        historyDTO.amount_u1 = size_base * (decimal)-0.5;
                    if (historyDTO.amount_u2 == 0)
                        historyDTO.amount_u2 = size_base * (decimal)-0.5;
                    if (historyDTO.amount_u3 == 0)
                        historyDTO.amount_u3 = size_base * (decimal)-0.5;
                    if (historyDTO.amount_u4 == 0)
                        historyDTO.amount_u4 = size_base * (decimal)-0.5;
                }
            }
            else if (historyDTO.loser == roomDTO.u1)
            {
                if (historyDTO.is_one_lose_all)
                    historyDTO.amount_u1 = size_base * (decimal)-1.50;
                else
                    historyDTO.amount_u1 = size_base * -1;
            }
            else if (historyDTO.loser == roomDTO.u2)
            {
                if (historyDTO.is_one_lose_all)
                    historyDTO.amount_u2 = size_base * (decimal)-1.50;
                else
                    historyDTO.amount_u2 = size_base * -1;
            }
            else if (historyDTO.loser == roomDTO.u3)
            {
                if (historyDTO.is_one_lose_all)
                    historyDTO.amount_u3 = size_base * (decimal)-1.50;
                else
                    historyDTO.amount_u3 = size_base * -1;
            }
            else if (historyDTO.loser == roomDTO.u4)
            {
                if (historyDTO.is_one_lose_all)
                    historyDTO.amount_u4 = size_base * (decimal)-1.50;
                else
                    historyDTO.amount_u4 = size_base * -1;
            }

            DateTime today = DateTime.Now;
            historyDTO.c_by = userDTO_authed.uid;
            historyDTO.c_date = today;
            historyDTO.u_by = userDTO_authed.uid;
            historyDTO.u_date = today;
            historyDTO.is_active = true;
            ////
            
            return historyDTO;
        }
    }
}
