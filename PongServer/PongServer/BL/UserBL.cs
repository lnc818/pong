﻿using PongServer.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PongServer.BL
{
    public static class UserBL
    {
        public static bool validateRoom(UserDTO userDTO)
        {
            try
            {
                if (userDTO.displayName.Trim() == "")
                {
                    throw new Exception("Missing user name");
                }
                if (userDTO.firebase_uid.Trim() == "")
                {
                    throw new Exception("Missing firebase uid");
                }
                if (userDTO.idToken.Trim() == "")
                {
                    throw new Exception("Missing firebase id token");
                }
                if (userDTO.refreshToken.Trim() == "")
                {
                    throw new Exception("Missing firebase refresh token");
                }



                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }
        public static List<UserDTO> maskUsers(List<UserDTO> userDTOs)
        {
            foreach(var userDTO in userDTOs)
            {
                userDTO.idToken = "";
                userDTO.refreshToken = "";
                userDTO.email = "";
            }
            return userDTOs;
        }
    }
}
