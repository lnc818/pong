﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PongServer.DTO
{
    public class UserDTO
    {
        public int uid { get; set; }
        public string firebase_uid { get; set; }
        public string displayName { get; set; }
        public string email { get; set; }
        public string photoURL { get; set; }
        public string idToken { get; set; }
        public string refreshToken { get; set; }
        public bool is_guest { get; set; }
        public bool is_active { get; set; }
        public DateTime last_login_date { get; set; }
    }
}
