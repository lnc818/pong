﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PongServer.DTO
{
    public class GroupUserDTO
    {
        public int uid { get; set; }
        public int group_uid { get; set; }
        public int user_uid { get; set; }
        public bool is_guest { get; set; }
        public bool is_active { get; set; }
        public int c_by { get; set; }
        public DateTime c_date { get; set; }
        public int u_by { get; set; }
        public DateTime u_date { get; set; }
        public UserDTO user { get; set; }
    }
}
