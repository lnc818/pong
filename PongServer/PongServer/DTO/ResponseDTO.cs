﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PongServer.DTO
{
    public enum ERROR_TYPE
    {
        NO_ERROR,
        API_PARA_ERROR,
        ARGUMENT_ERROR,
        EXPIRED
    }
    public class ResponseDTO
    {
        public bool isValid { get; set; }
        public ERROR_TYPE error_type { get;set; }
        public string message { get; set; }

        public ResponseDTO()
        {
            this.isValid = true;
            this.error_type = ERROR_TYPE.NO_ERROR;
            this.message = "";
        }
        public ResponseDTO(bool isValid, ERROR_TYPE error_type, string message)
        {
            this.isValid = isValid;
            this.error_type = error_type;
            this.message = message;
        }
    }
}
