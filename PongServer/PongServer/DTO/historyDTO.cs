﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PongServer.DTO
{
    public class HistoryDTO
    {
        public int uid { get; set; }
        public int room_uid { get; set; }
        public int winner { get; set; }
        public int loser { get; set; }
        public int size { get; set; }
        public decimal amount_u1 { get; set; }
        public decimal amount_u2 { get; set; }
        public decimal amount_u3 { get; set; }
        public decimal amount_u4 { get; set; }
        public bool is_one_lose_all { get; set; }
        public bool is_active { get; set; }
        public int c_by { get; set; }
        public DateTime c_date { get; set; }
        public int u_by { get; set; }
        public DateTime u_date { get; set; }
    }
}
