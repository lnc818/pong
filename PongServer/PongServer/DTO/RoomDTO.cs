﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PongServer.DTO
{
    public class RoomDTO
    {
        public int uid { get; set; }
        public int group_uid { get; set; }
        public string name { get; set; }
        public int u1 { get; set; }
        public int u2 { get; set; }
        public int u3 { get; set; }
        public int u4 { get; set; }
        public int min { get; set; }
        public int max { get; set; }
        public string size { get; set; }
        public List<decimal> size_list { get; set; }
        public bool is_active { get; set; }
        public int c_by { get; set; }
        public DateTime c_date { get; set; }
        public int u_by { get; set; }
        public DateTime u_date { get; set; }
    }
}
