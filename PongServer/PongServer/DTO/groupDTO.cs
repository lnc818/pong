﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PongServer.DTO
{
    public class GroupDTO
    {
        public int uid { get; set; }
        public string name { get; set; }
        public bool is_active { get; set; }
        public int c_by { get; set; }
        public DateTime c_date { get; set; }
        public int u_by { get; set; }
        public DateTime u_date { get; set; }
    }
}
