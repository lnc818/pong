﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FirebaseAdmin.Auth;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PongServer.BL;
using PongServer.Common;
using PongServer.DAL;
using PongServer.DTO;

namespace PongServer.Controllers.Group
{
    [Route("api/[controller]")]
    public class GroupController : Controller
    {
        // GET api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2", "value3" };
        }

        //[Route("[action]")]
        //[HttpGet]
        //public IEnumerable<GroupDTO> ListGroup()
        //{
        //    List<GroupDTO> groupDTOs = GroupDAL.getGroups();
        //    return groupDTOs;
        //    //return new string[] { "ListProducts", "value2", "value3" };
        //}
        [Route("[action]")]
        [HttpPost]
        public async Task<IEnumerable<UserDTO>> GetUsersByGroup([FromBody]JObject data)
        {
            UserDTO userDTO = new UserDTO();
            GroupDTO groupDTO = new GroupDTO();
            RoomDTO roomDTO = new RoomDTO();
            try
            {
                userDTO = data["user"].ToObject<UserDTO>();
                groupDTO = data["current_group"].ToObject<GroupDTO>();
            }
            catch (Exception ex)
            {
                ResponseDTO responseDTO = new ResponseDTO(false, ERROR_TYPE.API_PARA_ERROR, "API_PARA_ERROR: " + ex.Message);
                throw new Exception(responseDTO.message);
            }

            ////auth
            FireBaseConfiguration fireBaseConfiguration = FireBaseConfiguration.Instance;
            UserDTO userDTO_authed = await Auth.GetUserByToken(userDTO.idToken);
            if (userDTO_authed == null)
                throw new Exception("You are not authorized.");
            ////

            List<UserDTO> userDTOs = UserDAL.getUserByGroup(groupDTO.uid);
            userDTOs = UserBL.maskUsers(userDTOs);
            return userDTOs;
        }


        [Route("[action]")]
        [HttpPost]
        public async Task<IEnumerable<GroupDTO>> GetGroups([FromBody]JObject data)
        {
            UserDTO userDTO = data["user"].ToObject<UserDTO>();

            FireBaseConfiguration fireBaseConfiguration = FireBaseConfiguration.Instance;

            UserDTO userDTO_authed = await Auth.GetUserByToken(userDTO.idToken);

            List<GroupDTO> groupDTOs = GroupDAL.getGroupByUser(userDTO_authed);
            return groupDTOs;
            //return new string[] { "ListProducts", "value2", "value3" };
        }

        [Route("[action]")]
        [HttpPost]
        public IEnumerable<GroupDTO> AddGroup([FromBody]JObject data)
        {
            GroupDTO groupDTO = data["group"].ToObject<GroupDTO>();
            UserDTO userDTO = data["user"].ToObject<UserDTO>();

            DateTime Now = DateTime.Now;

            ////Add group
            int group_uid = 0;
            {
                groupDTO.c_by = userDTO.uid;
                groupDTO.c_date = Now;
                groupDTO.u_by = userDTO.uid;
                groupDTO.u_date = Now;
                groupDTO.is_active = true;

                group_uid = GroupDAL.addGroup(groupDTO);
                if (group_uid <= 0)
                    throw new Exception("Error in creating group");
            }
            ////

            //// Add groupUser Mapping
            int group_user_uid = 0;
            {
                GroupUserDTO groupUserDTO = new GroupUserDTO();
                groupUserDTO.group_uid = groupDTO.uid;
                groupUserDTO.user_uid = userDTO.uid;
                groupUserDTO.is_guest = false;
                groupUserDTO.is_active = true;
                groupUserDTO.c_by = userDTO.uid;
                groupUserDTO.c_date = Now;
                groupUserDTO.u_by = userDTO.uid;
                groupUserDTO.u_date = Now;

                group_user_uid = GroupUserDAL.addGroupUser(groupUserDTO);
                if (group_user_uid <= 0)
                    throw new Exception("Error in mapping group user");
            }
            ////

            List<GroupDTO> groupDTOs = GroupDAL.getGroupByUser(userDTO);
            return groupDTOs;
            //return new string[] { "ListProducts", "value2", "value3" };
        }

    }
    public class User
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
    }
}