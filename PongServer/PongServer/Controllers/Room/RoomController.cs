﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FirebaseAdmin.Auth;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PongServer.BL;
using PongServer.Common;
using PongServer.DAL;
using PongServer.DTO;

namespace PongServer.Controllers.Group
{
    [Route("api/[controller]")]
    public class RoomController : Controller
    {
        // GET api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2", "value3" };
        }
        [Route("[action]")]
        [HttpPost]
        public async Task<IEnumerable<UserDTO>> GetUsersByRoom([FromBody]JObject data)
        {
            UserDTO userDTO = new UserDTO();
            RoomDTO roomDTO = new RoomDTO();
            try
            {
                userDTO = data["user"].ToObject<UserDTO>();
                roomDTO = data["current_room"].ToObject<RoomDTO>();
            }
            catch (Exception ex)
            {
                ResponseDTO responseDTO = new ResponseDTO(false, ERROR_TYPE.API_PARA_ERROR, "API_PARA_ERROR: " + ex.Message);
                throw new Exception(responseDTO.message);
            }

            ////auth
            FireBaseConfiguration fireBaseConfiguration = FireBaseConfiguration.Instance;
            UserDTO userDTO_authed = await Auth.GetUserByToken(userDTO.idToken);
            if (userDTO_authed == null)
                throw new Exception("You are not authorized.");
            ////

            List<UserDTO> userDTOs = UserDAL.getUserByRoom(roomDTO.uid);
            userDTOs = UserBL.maskUsers(userDTOs);
            return userDTOs;
        }


        [Route("[action]")]
        [HttpPost]
        public async Task<IEnumerable<RoomDTO>> GetRooms([FromBody]JObject data)
        {
            UserDTO userDTO = new UserDTO();
            GroupDTO groupDTO = new GroupDTO();
            try
            {
                userDTO = data["user"].ToObject<UserDTO>();
                groupDTO = data["current_group"].ToObject<GroupDTO>();
            }
            catch (Exception ex)
            {
                ResponseDTO responseDTO = new ResponseDTO(false, ERROR_TYPE.API_PARA_ERROR, "API_PARA_ERROR: " + ex.Message);
                throw new Exception(responseDTO.message);
            }

            ////auth
            FireBaseConfiguration fireBaseConfiguration = FireBaseConfiguration.Instance;
            UserDTO userDTO_authed = await Auth.GetUserByToken(userDTO.idToken);
            if (userDTO_authed == null)
                throw new Exception("You are not authorized.");
            ////

            List<RoomDTO> roomDTOs = RoomDAL.getRoomsByGroup(groupDTO.uid);
            return roomDTOs;
        }

        [Route("[action]")]
        [HttpPost]
        public async Task<IEnumerable<HistoryDTO>> AddRoom([FromBody]JObject data)
        {
            UserDTO userDTO = new UserDTO();
            RoomDTO roomDTO = new RoomDTO();
            HistoryDTO historyDTO = new HistoryDTO();
            try
            {
                userDTO = data["user"].ToObject<UserDTO>();
                roomDTO = data["current_room"].ToObject<RoomDTO>();
                historyDTO = data["history"].ToObject<HistoryDTO>();
            }
            catch (Exception ex)
            {
                ResponseDTO responseDTO = new ResponseDTO(false, ERROR_TYPE.API_PARA_ERROR, "API_PARA_ERROR: " + ex.Message);
                throw new Exception(responseDTO.message);
            }

            ////auth
            FireBaseConfiguration fireBaseConfiguration = FireBaseConfiguration.Instance;
            UserDTO userDTO_authed = await Auth.GetUserByToken(userDTO.idToken);
            if (userDTO_authed == null)
                throw new Exception("You are not authorized.");
            ////

            ////Update Room info
            DateTime today = DateTime.Now;
            historyDTO.c_by = userDTO_authed.uid;
            historyDTO.c_date = today;
            historyDTO.u_by = userDTO_authed.uid;
            historyDTO.u_date = today;
            historyDTO.is_active = true;
            ////

            int history_uid = HistoryDAL.addHistory(historyDTO);
            if (history_uid == 0)
                throw new Exception("Add history fail");

            List<HistoryDTO> historiesDTOs = HistoryDAL.GetHistoriesByRoom(roomDTO.uid);
            return historiesDTOs;
        }

        
    }
}