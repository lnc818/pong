﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FirebaseAdmin.Auth;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using PongServer.Common;
using PongServer.DAL;
using PongServer.DTO;

namespace PongServer.Controllers.Group
{
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        // GET api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2", "value3" };
        }

        [Route("[action]")]
        [HttpGet]
        public IEnumerable<UserDTO> ListUsers()
        {
            List<UserDTO> userDTOs = new List<UserDTO>();
            userDTOs.Add(UserDAL.getUser(1));
            return userDTOs;
            //return new string[] { "ListProducts", "value2", "value3" };
        }

        [Route("[action]")]
        [HttpPost]
        public async Task<UserDTO> LoginUser([FromBody] UserDTO userDTO)
        {
            //FireBaseConfiguration fireBaseConfiguration = FireBaseConfiguration.Instance;
            UserDTO userDTO_authed = await Auth.GetUserByToken(userDTO.idToken);

            UserDTO u = UserDAL.getUser(userDTO.firebase_uid);
            if (u == null)
            {
                userDTO.is_guest = false;
                int uid = UserDAL.addUser(userDTO);

                userDTO_authed = UserDAL.getUser(uid);
            }
            else
            {
                userDTO_authed.idToken = userDTO.idToken;
                userDTO_authed.refreshToken = userDTO.refreshToken;
                userDTO_authed.last_login_date = DateTime.Now;
                UserDAL.updateUser(userDTO_authed);
            }

            return userDTO_authed;

            //UserDTO u = UserDAL.getUser(userDTO.firebase_uid);
            //if (u == null)
            //{
            //    int uid = UserDAL.addUser(userDTO);

            //    u = UserDAL.getUser(uid);
            //}
            //else
            //{
            //    UserDAL.updateUserLastLoginDate(u);
            //    u = UserDAL.getUser(u.uid);
            //}
            //return u;
        }

        [Route("[action]")]
        [HttpPost]
        public async Task<List<GroupUserDTO>> AddGuestUser([FromBody]JObject data)
        {
            UserDTO userDTO = new UserDTO();
            UserDTO guestUserDTO = new UserDTO();
            GroupDTO groupDTO = new GroupDTO();

            try
            {
                userDTO = data["user"].ToObject<UserDTO>();
                guestUserDTO = data["guest_user"].ToObject<UserDTO>();
                groupDTO = data["current_group"].ToObject<GroupDTO>();
            }
            catch (Exception ex)
            {
                ResponseDTO responseDTO = new ResponseDTO(false, ERROR_TYPE.API_PARA_ERROR, "API_PARA_ERROR: " + ex.Message);
                throw new Exception(responseDTO.message);
            }

            ////auth
            FireBaseConfiguration fireBaseConfiguration = FireBaseConfiguration.Instance;
            UserDTO userDTO_authed = await Auth.GetUserByToken(userDTO.idToken);
            if (userDTO_authed == null)
                throw new Exception("You are not authorized.");
            ////

            DateTime Now = DateTime.Now;

            int guest_user_uid = 0;
            ////Add guest to user table
            {
                guestUserDTO.firebase_uid = Guid.NewGuid().ToString();
                guestUserDTO.email = "GUEST";
                guestUserDTO.idToken = "GUEST";
                guestUserDTO.refreshToken = "GUEST";
                guestUserDTO.is_active = true;
                guestUserDTO.is_guest = true;
                guestUserDTO.last_login_date = Now;
                guest_user_uid = UserDAL.addUser(guestUserDTO);

                if (guest_user_uid <= 0)
                    throw new Exception("Error in creating guest user");
            }
            ////

            //// Add groupUser Mapping
            int group_user_uid = 0;
            {
                GroupUserDTO groupUserDTO = new GroupUserDTO();
                groupUserDTO.group_uid = groupDTO.uid;
                groupUserDTO.user_uid = guest_user_uid;
                groupUserDTO.is_guest = true;
                groupUserDTO.is_active = true;
                groupUserDTO.c_by = userDTO_authed.uid;
                groupUserDTO.c_date = Now;
                groupUserDTO.u_by = userDTO_authed.uid;
                groupUserDTO.u_date = Now;

                group_user_uid = GroupUserDAL.addGroupUser(groupUserDTO);
                if (group_user_uid <= 0)
                    throw new Exception("Error in mapping group user");
            }
            ////

            ////Get groupUser list by group uid
            List<GroupUserDTO> groupUserDTOs = new List<GroupUserDTO>();
            {
                groupUserDTOs = GroupUserDAL.getGroupUsersByGroup(groupDTO.uid);
            }
            ////
            
            return groupUserDTOs;
        }
    }
}