﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FirebaseAdmin;
using FirebaseAdmin.Auth;
using Google.Apis.Auth.OAuth2;
using Microsoft.AspNetCore.Mvc;
using PongServer.Common;
using PongServer.DTO;

namespace PongServer.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        // GET api/values
        [HttpGet]
        public async Task<IEnumerable<string>> GetAsync()
        {
            FireBaseConfiguration fireBaseConfiguration = FireBaseConfiguration.Instance;
            UserRecord userRecord = await FirebaseAuth.DefaultInstance.GetUserByEmailAsync("leeb4b4@hotmail.com");

            UserDTO userDTO = await Auth.GetUserByToken("AEu4IL35MIRdaIxjW_MowIJGcucPkdzygczMR8dmNv6IDXgqqTw9T2bmRpD0OOqhQ2REQ8AxPljj4WUfd2Q7zi9eBXDg_60lMaVXjdDbu1-sr31xLXiH3L-Zla46AoipO2J0knbjIFBe-aa0aIlgU5ojCZh0rA0taptsly9xcPtdV7y95FTbo9WOQgDETdeD4wHw2SL9zDt_gGxjVcTMQl9gKNYVcBx8lf_uEOLL1djlxZHFL6EaAnslMDaa4cp1fzBgG3NVN4EBiIt8139wyX2yz6QkBzPOztTqTuo1fChDV3bEGFZvzRc");

            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
