﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FirebaseAdmin.Auth;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PongServer.BL;
using PongServer.Common;
using PongServer.DAL;
using PongServer.DTO;

namespace PongServer.Controllers.Group
{
    [Route("api/[controller]")]
    public class HistoryController : Controller
    {
        // GET api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2", "value3" };
        }
        [Route("[action]")]
        [HttpPost]
        public async Task<IEnumerable<HistoryDTO>> GetHistories([FromBody]JObject data)
        {
            UserDTO userDTO = new UserDTO();
            RoomDTO roomDTO = new RoomDTO();
            try
            {
                userDTO = data["user"].ToObject<UserDTO>();
                roomDTO = data["current_room"].ToObject<RoomDTO>();
            }
            catch (Exception ex)
            {
                ResponseDTO responseDTO = new ResponseDTO(false, ERROR_TYPE.API_PARA_ERROR, "API_PARA_ERROR: " + ex.Message);
                throw new Exception(responseDTO.message);
            }

            ////auth
            FireBaseConfiguration fireBaseConfiguration = FireBaseConfiguration.Instance;
            UserDTO userDTO_authed = await Auth.GetUserByToken(userDTO.idToken);
            if (userDTO_authed == null)
                throw new Exception("You are not authorized.");
            ////

            List<HistoryDTO> historiesDTOs = HistoryDAL.GetHistoriesByRoom(roomDTO.uid);
            return historiesDTOs;
        }



        [Route("[action]")]
        [HttpPost]
        public async Task<IEnumerable<HistoryDTO>> AddHistory([FromBody]JObject data)
        {
            UserDTO userDTO = new UserDTO();
            RoomDTO roomDTO = new RoomDTO();
            HistoryDTO historyDTO = new HistoryDTO();
            try
            {
                userDTO = data["user"].ToObject<UserDTO>();
                roomDTO = data["current_eoom"].ToObject<RoomDTO>();
                historyDTO = data["history"].ToObject<HistoryDTO>();
            }
            catch (Exception ex)
            {
                ResponseDTO responseDTO = new ResponseDTO(false, ERROR_TYPE.API_PARA_ERROR, "API_PARA_ERROR: " + ex.Message);
                throw new Exception(responseDTO.message);
            }

            ////auth
            FireBaseConfiguration fireBaseConfiguration = FireBaseConfiguration.Instance;
            UserDTO userDTO_authed = await Auth.GetUserByToken(userDTO.idToken);
            if (userDTO_authed == null)
                throw new Exception("You are not authorized.");
            ////

            ////Update history info
            roomDTO = RoomDAL.getRoom(roomDTO.uid);

            if (!HistoryBL.validateHistory(historyDTO, roomDTO))
                throw new Exception("Invalid history");

            historyDTO = HistoryBL.CalHistoryFromUserInput(historyDTO, roomDTO, userDTO_authed);

            int history_uid = HistoryDAL.addHistory(historyDTO);
            if (history_uid == 0)
                throw new Exception("Add history fail");

            List<HistoryDTO> historyDTOs = HistoryDAL.GetHistoriesByRoom(roomDTO.uid);
            return historyDTOs;
        }

        
    }
}