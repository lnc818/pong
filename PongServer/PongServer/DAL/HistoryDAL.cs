﻿using PongServer.Common;
using PongServer.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace PongServer.DAL
{
    public class HistoryDAL
    {
        public static List<HistoryDTO> GetHistoriesByRoom(int room_uid)
        {
            string connetionString;

            AppConfiguration appConfiguration = new AppConfiguration();
            connetionString = appConfiguration.ConnectionString;

            using (SqlConnection conn = new SqlConnection(connetionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.Connection.Open();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = @"SELECT [uid]
                                          ,[room_uid]
                                          ,[winner]
                                          ,[loser]
                                          ,[size]
                                          ,[amount_u1]
                                          ,[amount_u2]
                                          ,[amount_u3]
                                          ,[amount_u4]
                                          ,[is_one_lose_all]
                                          ,[is_active]
                                          ,[c_by]
                                          ,[c_date]
                                          ,[u_by]
                                          ,[u_date]
                                      FROM [dbo].[history]
                                    where room_uid = @room_uid
                                    AND is_active = 1 
                                    ORDER BY [c_date] DESC";

                SqlDataAdapter da = new SqlDataAdapter(cmd);

                cmd.Parameters.AddWithValue("@room_uid", room_uid);

                DataTable dt = new DataTable();

                da.Fill(dt);

                List<HistoryDTO> historyDTOs = new List<HistoryDTO>();
                historyDTOs = Utility.ConvertDataTable<HistoryDTO>(dt);

                return historyDTOs;
            }
        }

        public static int addHistory(HistoryDTO historyDTO)
        {
            string connetionString;

            AppConfiguration appConfiguration = new AppConfiguration();
            connetionString = appConfiguration.ConnectionString;

            using (SqlConnection conn = new SqlConnection(connetionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.Connection.Open();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = @"INSERT INTO [dbo].[history]
                                          ([room_uid]
                                          ,[winner]
                                          ,[loser]
                                          ,[size]
                                          ,[amount_u1]
                                          ,[amount_u2]
                                          ,[amount_u3]
                                          ,[amount_u4]
                                          ,[is_one_lose_all]
                                          ,[is_active]
                                          ,[c_by]
                                          ,[c_date]
                                          ,[u_by]
                                          ,[u_date])
                                    VALUES
                                          (@room_uid
                                          ,@winner
                                          ,@loser
                                          ,@size
                                          ,@amount_u1
                                          ,@amount_u2
                                          ,@amount_u3
                                          ,@amount_u4
                                          ,@is_one_lose_all
                                          ,@is_active
                                          ,@c_by
                                          ,@c_date
                                          ,@u_by
                                          ,@u_date)
                                    SELECT SCOPE_IDENTITY()";

                cmd.Parameters.AddWithValue("@room_uid", historyDTO.room_uid);
                cmd.Parameters.AddWithValue("@winner", historyDTO.winner);
                cmd.Parameters.AddWithValue("@loser", historyDTO.loser);
                cmd.Parameters.AddWithValue("@size", historyDTO.size);
                cmd.Parameters.AddWithValue("@amount_u1", historyDTO.amount_u1);
                cmd.Parameters.AddWithValue("@amount_u2", historyDTO.amount_u2);
                cmd.Parameters.AddWithValue("@amount_u3", historyDTO.amount_u3);
                cmd.Parameters.AddWithValue("@amount_u4", historyDTO.amount_u4);
                cmd.Parameters.AddWithValue("@is_one_lose_all", historyDTO.is_one_lose_all);
                cmd.Parameters.AddWithValue("@is_active", 1);
                cmd.Parameters.AddWithValue("@c_by", historyDTO.c_by);
                cmd.Parameters.AddWithValue("@c_date", historyDTO.c_date);
                cmd.Parameters.AddWithValue("@u_by", historyDTO.u_by);
                cmd.Parameters.AddWithValue("@u_date", historyDTO.u_date);



                int uid = Convert.ToInt32(cmd.ExecuteScalar());

                return uid;
            }
        }
        public static void deleteHistory(HistoryDTO historyDTO, UserDTO userDTO_authed)
        {
            string connetionString;

            AppConfiguration appConfiguration = new AppConfiguration();
            connetionString = appConfiguration.ConnectionString;

            using (SqlConnection conn = new SqlConnection(connetionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.Connection.Open();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = @"UPDATE [dbo].[history]
                                          SET [is_active] = 0
                                          ,[u_by] = @user_uid
                                          ,[u_date] = GETDATE()
                                    WHERE [uid] = @uid";

                cmd.Parameters.AddWithValue("@uid", historyDTO.uid);
                cmd.Parameters.AddWithValue("@user_uid", userDTO_authed.uid);




                cmd.ExecuteNonQuery();

                return;
            }
        }
    }
}
