﻿using PongServer.Common;
using PongServer.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace PongServer.DAL
{
    public static class UserDAL
    {
        public static UserDTO getUser(int uid)
        {
            string connetionString;

            AppConfiguration appConfiguration = new AppConfiguration();
            connetionString = appConfiguration.ConnectionString;

            using (SqlConnection conn = new SqlConnection(connetionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.Connection.Open();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = @"SELECT * FROM [dbo].[user]
                                    WHERE uid = @uid";
                cmd.Parameters.AddWithValue("@uid", uid);


                SqlDataAdapter da = new SqlDataAdapter(cmd);

                DataTable dt = new DataTable();

                da.Fill(dt);

                List<UserDTO> userDTOs = new List<UserDTO>();
                userDTOs = Utility.ConvertDataTable<UserDTO>(dt);

                if (userDTOs.Count == 0)
                    return null;
                else
                    return userDTOs[0];
            }
        }
        public static UserDTO getUser(string firebase_uid)
        {
            string connetionString;

            AppConfiguration appConfiguration = new AppConfiguration();
            connetionString = appConfiguration.ConnectionString;

            using (SqlConnection conn = new SqlConnection(connetionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.Connection.Open();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = @"SELECT * FROM [dbo].[user]
                                    WHERE firebase_uid = @firebase_uid";
                cmd.Parameters.AddWithValue("@firebase_uid", firebase_uid);


                SqlDataAdapter da = new SqlDataAdapter(cmd);

                DataTable dt = new DataTable();

                da.Fill(dt);

                List<UserDTO> userDTOs = new List<UserDTO>();
                userDTOs = Utility.ConvertDataTable<UserDTO>(dt);

                if (userDTOs.Count == 0)
                    return null;
                else
                    return userDTOs[0];
            }
        }

        public static List<UserDTO> getUserByGroup(int group_uid)
        {
            string connetionString;

            AppConfiguration appConfiguration = new AppConfiguration();
            connetionString = appConfiguration.ConnectionString;

            using (SqlConnection conn = new SqlConnection(connetionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.Connection.Open();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = @"SELECT b.* FROM [dbo].[groupUser] as a 
                                    LEFT JOIN [dbo].[user] as b 
                                    on a.user_uid = b.uid
                                    WHERE a.group_uid = @group_uid
                                    AND a.is_active = 1";
                cmd.Parameters.AddWithValue("@group_uid", group_uid);


                SqlDataAdapter da = new SqlDataAdapter(cmd);

                DataTable dt = new DataTable();

                da.Fill(dt);

                List<UserDTO> userDTOs = new List<UserDTO>();
                userDTOs = Utility.ConvertDataTable<UserDTO>(dt);

                return userDTOs;
            }
        }
        public static List<UserDTO> getUserByRoom(int room_uid)
        {
            string connetionString;

            AppConfiguration appConfiguration = new AppConfiguration();
            connetionString = appConfiguration.ConnectionString;

            using (SqlConnection conn = new SqlConnection(connetionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.Connection.Open();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = @"SELECT * FROM [user] where uid IN (
                                    SELECT u1 AS player_uid FROM [room] WHERE uid = @room_uid
                                    UNION
                                    SELECT u2 AS player_uid FROM [room] WHERE uid = @room_uid
                                    UNION
                                    SELECT u3 AS player_uid FROM [room] WHERE uid = @room_uid
                                    UNION
                                    SELECT u4 AS player_uid FROM [room] WHERE uid = @room_uid
                                    )";
                cmd.Parameters.AddWithValue("@room_uid", room_uid);


                SqlDataAdapter da = new SqlDataAdapter(cmd);

                DataTable dt = new DataTable();

                da.Fill(dt);

                List<UserDTO> userDTOs = new List<UserDTO>();
                userDTOs = Utility.ConvertDataTable<UserDTO>(dt);

                return userDTOs;
            }
        }

        public static int addUser(UserDTO userDTO)
        {
            string connetionString;

            AppConfiguration appConfiguration = new AppConfiguration();
            connetionString = appConfiguration.ConnectionString;

            using (SqlConnection conn = new SqlConnection(connetionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.Connection.Open();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = @"INSERT INTO [dbo].[user]
                                           ([firebase_uid]
                                           ,[displayName]
                                           ,[email]
                                           ,[photoURL]
                                           ,[idToken]
                                           ,[refreshToken]
                                           ,[is_guest]
                                           ,[is_active]
                                           ,[last_login_date])
                                     VALUES
                                           (@firebase_uid
                                           ,@displayName
                                           ,@email
                                           ,@photoURL
                                           ,@idToken
                                           ,@refreshToken
                                           ,@is_guest
                                           ,@is_active
                                           ,@last_login_date)
                                    SELECT SCOPE_IDENTITY()";

                cmd.Parameters.AddWithValue("@firebase_uid", userDTO.firebase_uid);
                cmd.Parameters.AddWithValue("@displayName", userDTO.displayName);
                cmd.Parameters.AddWithValue("@email", userDTO.email);
                cmd.Parameters.AddWithValue("@photoURL", userDTO.photoURL);
                cmd.Parameters.AddWithValue("@idToken", userDTO.idToken);
                cmd.Parameters.AddWithValue("@refreshToken", userDTO.refreshToken);
                cmd.Parameters.AddWithValue("@is_guest", userDTO.is_guest);
                cmd.Parameters.AddWithValue("@is_active", 1);
                cmd.Parameters.AddWithValue("@last_login_date", DateTime.Now);


                int uid = Convert.ToInt32(cmd.ExecuteScalar());

                return uid;
            }
        }

        public static int updateUserLastLoginDate(UserDTO userDTO)
        {
            string connetionString;

            AppConfiguration appConfiguration = new AppConfiguration();
            connetionString = appConfiguration.ConnectionString;

            using (SqlConnection conn = new SqlConnection(connetionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.Connection.Open();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = @"UPDATE [dbo].[user]
                                      SET 
                                         [last_login_date] = @last_login_date
                                    WHERE uid = @uid";

                cmd.Parameters.AddWithValue("@uid", userDTO.uid);
                cmd.Parameters.AddWithValue("@last_login_date", DateTime.Now);


                int uid = Convert.ToInt32(cmd.ExecuteScalar());

                return uid;
            }
        }

        public static void updateUser(UserDTO userDTO)
        {
            string connetionString;

            AppConfiguration appConfiguration = new AppConfiguration();
            connetionString = appConfiguration.ConnectionString;

            using (SqlConnection conn = new SqlConnection(connetionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.Connection.Open();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = @"UPDATE [dbo].[user]
                                      SET 
                                            [firebase_uid] = @firebase_uid,
                                            [displayName] = @displayName,
                                            [email] = @email,
                                            [photoURL] = @photoURL,
                                            [idToken] = @idToken,
                                            [refreshToken] = @refreshToken,
                                            [is_active] = @is_active,
                                            [last_login_date] = @last_login_date

                                    WHERE uid = @uid";

                cmd.Parameters.AddWithValue("@uid", userDTO.uid);
                cmd.Parameters.AddWithValue("@firebase_uid", userDTO.firebase_uid);
                cmd.Parameters.AddWithValue("@displayName", userDTO.displayName);
                cmd.Parameters.AddWithValue("@email", userDTO.email);
                cmd.Parameters.AddWithValue("@photoURL", userDTO.photoURL);
                cmd.Parameters.AddWithValue("@idToken", userDTO.idToken);
                cmd.Parameters.AddWithValue("@refreshToken", userDTO.refreshToken);
                cmd.Parameters.AddWithValue("@is_active", userDTO.is_active);
                cmd.Parameters.AddWithValue("@last_login_date", DateTime.Now);

                cmd.ExecuteNonQuery();

                return;
            }
        }
    }
}
