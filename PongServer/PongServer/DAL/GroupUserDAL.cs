﻿using PongServer.Common;
using PongServer.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace PongServer.DAL
{
    public class GroupUserDAL
    {
        public static List<GroupUserDTO> getGroupUsersByGroup(int group_uid)
        {
            string connetionString;

            AppConfiguration appConfiguration = new AppConfiguration();
            connetionString = appConfiguration.ConnectionString;

            using (SqlConnection conn = new SqlConnection(connetionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.Connection.Open();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = @"SELECT [uid]
                                        ,[group_uid]
                                        ,[user_uid]
                                        ,[is_guest]
                                        ,[is_active]
                                        ,[c_by]
                                        ,[c_date]
                                        ,[u_by]
                                        ,[u_date]
                                    FROM [dbo].[groupUser]
                                    WHERE group_uid = @group_uid
                                    AND is_active = 1";


                SqlDataAdapter da = new SqlDataAdapter(cmd);

                cmd.Parameters.AddWithValue("@group_uid", group_uid);

                DataTable dt = new DataTable();

                da.Fill(dt);

                List<GroupUserDTO> groupUserDTOs = new List<GroupUserDTO>();
                groupUserDTOs = Utility.ConvertDataTable<GroupUserDTO>(dt);

                return groupUserDTOs;
            }
        }
        public static int addGroupUser(GroupUserDTO groupUserDTO)
        {
            string connetionString;

            AppConfiguration appConfiguration = new AppConfiguration();
            connetionString = appConfiguration.ConnectionString;

            using (SqlConnection conn = new SqlConnection(connetionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.Connection.Open();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = @"INSERT INTO [dbo].[groupUser]
                                           ([group_uid]
                                            ,[user_uid]
                                            ,[is_guest]
                                            ,[is_active]
                                            ,[c_by]
                                            ,[c_date]
                                            ,[u_by]
                                            ,[u_date])
                                     VALUES
                                           (@group_uid
                                           ,@user_uid
                                           ,@is_guest
                                           ,@is_active
                                           ,@c_by
                                           ,@c_date
                                           ,@u_by
                                           ,@u_date)
                                    SELECT SCOPE_IDENTITY()";

                cmd.Parameters.AddWithValue("@group_uid", groupUserDTO.group_uid);
                cmd.Parameters.AddWithValue("@user_uid", groupUserDTO.user_uid);
                cmd.Parameters.AddWithValue("@is_guest", groupUserDTO.is_guest);
                cmd.Parameters.AddWithValue("@is_active", groupUserDTO.is_active);
                cmd.Parameters.AddWithValue("@c_by", groupUserDTO.c_by);
                cmd.Parameters.AddWithValue("@c_date", groupUserDTO.c_date);
                cmd.Parameters.AddWithValue("@u_by", groupUserDTO.u_by);
                cmd.Parameters.AddWithValue("@u_date", groupUserDTO.u_by);


                int uid = Convert.ToInt32(cmd.ExecuteScalar());

                return uid;
            }
        }

    }
}
