﻿using PongServer.Common;
using PongServer.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace PongServer.DAL
{
    public static class GroupDAL
    {
        public static List<GroupDTO> getGroupByUser(UserDTO userDTO)
        {
            string connetionString;
            SqlConnection cnn;

            AppConfiguration appConfiguration = new AppConfiguration();
            connetionString = appConfiguration.ConnectionString;

            using (SqlConnection conn = new SqlConnection(connetionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.Connection.Open();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT * FROM [group] WHERE is_active = 1 and c_by = @user_uid";


                SqlDataAdapter da = new SqlDataAdapter(cmd);

                cmd.Parameters.AddWithValue("@user_uid", userDTO.uid);

                DataTable dt = new DataTable();

                da.Fill(dt);

                List<GroupDTO> groupDTOs = new List<GroupDTO>();
                groupDTOs = Utility.ConvertDataTable<GroupDTO>(dt);

                return groupDTOs;
            }
        }

        public static int addGroup(GroupDTO groupDTO)
        {
            string connetionString;
            SqlConnection cnn;

            AppConfiguration appConfiguration = new AppConfiguration();
            connetionString = appConfiguration.ConnectionString;

            using (SqlConnection conn = new SqlConnection(connetionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.Connection.Open();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = @"INSERT INTO [dbo].[group]
                                          ([name]
                                          ,[is_active]
                                          ,[c_by]
                                          ,[c_date]
                                          ,[u_by]
                                          ,[u_date])
                                    VALUES
                                          (@name
                                          ,1
                                          ,@c_by
                                          ,@c_date
                                          ,@u_by
                                          ,@u_date)
                                    SELECT SCOPE_IDENTITY()";

                cmd.Parameters.AddWithValue("@name", groupDTO.name);
                cmd.Parameters.AddWithValue("@c_by", groupDTO.c_by);
                cmd.Parameters.AddWithValue("@c_date", groupDTO.c_date);
                cmd.Parameters.AddWithValue("@u_by", groupDTO.u_by);
                cmd.Parameters.AddWithValue("@u_date", groupDTO.u_date);

                int uid = Convert.ToInt32(cmd.ExecuteScalar());

                return uid;
            }
        }
    }
}
