﻿using PongServer.Common;
using PongServer.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace PongServer.DAL
{
    public class RoomDAL
    {
        public static RoomDTO getRoom(int room_uid)
        {
            string connetionString;

            AppConfiguration appConfiguration = new AppConfiguration();
            connetionString = appConfiguration.ConnectionString;

            using (SqlConnection conn = new SqlConnection(connetionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.Connection.Open();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = @"SELECT [uid]
                                        ,[group_uid]
                                        ,[name]
                                        ,[u1]
                                        ,[u2]
                                        ,[u3]
                                        ,[u4]
                                        ,[min]
                                        ,[max]
                                        ,[size]
                                        ,[is_active]
                                        ,[c_by]
                                        ,[c_date]
                                        ,[u_by]
                                        ,[u_date]
                                          FROM[dbo].[room]
                                          WHERE uid = @room_uid
                                    AND is_active = 1";


                SqlDataAdapter da = new SqlDataAdapter(cmd);

                cmd.Parameters.AddWithValue("@room_uid", room_uid);

                DataTable dt = new DataTable();

                da.Fill(dt);

                if (dt.Rows.Count != 1)
                    return null;

                RoomDTO roomDTO = new RoomDTO();
                roomDTO = Utility.ConvertDataTable<RoomDTO>(dt)[0];

                return roomDTO;
            }
        }

        public static List<RoomDTO> getRoomsByGroup(int group_uid)
        {
            string connetionString;

            AppConfiguration appConfiguration = new AppConfiguration();
            connetionString = appConfiguration.ConnectionString;

            using (SqlConnection conn = new SqlConnection(connetionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.Connection.Open();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = @"SELECT [uid]
                                        ,[group_uid]
                                        ,[name]
                                        ,[u1]
                                        ,[u2]
                                        ,[u3]
                                        ,[u4]
                                        ,[min]
                                        ,[max]
                                        ,[size]
                                        ,[is_active]
                                        ,[c_by]
                                        ,[c_date]
                                        ,[u_by]
                                        ,[u_date]
                                          FROM[dbo].[room]
                                          WHERE group_uid = @group_uid
                                    AND is_active = 1";


                SqlDataAdapter da = new SqlDataAdapter(cmd);

                cmd.Parameters.AddWithValue("@group_uid", group_uid);

                DataTable dt = new DataTable();

                da.Fill(dt);

                List<RoomDTO> roomDTOs = new List<RoomDTO>();
                roomDTOs = Utility.ConvertDataTable<RoomDTO>(dt);

                return roomDTOs;
            }
        }

        public static int addRoom(RoomDTO roomDTO)
        {
            string connetionString;

            AppConfiguration appConfiguration = new AppConfiguration();
            connetionString = appConfiguration.ConnectionString;

            using (SqlConnection conn = new SqlConnection(connetionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.Connection.Open();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = @"INSERT INTO [dbo].[room]
                                           ([group_uid]
                                           ,[name]
                                           ,[u1]
                                           ,[u2]
                                           ,[u3]
                                           ,[u4]
                                           ,[min]
                                           ,[max]
                                           ,[size]
                                           ,[is_active]
                                           ,[c_by]
                                           ,[c_date]
                                           ,[u_by]
                                           ,[u_date])
                                     VALUES
                                           (@group_uid
                                           ,@name
                                           ,@u1
                                           ,@u2
                                           ,@u3
                                           ,@u4
                                           ,@min
                                           ,@max
                                           ,@size
                                           ,@is_active
                                           ,@c_by
                                           ,@c_date
                                           ,@u_by
                                           ,@u_date)
                                    SELECT SCOPE_IDENTITY()";


                SqlDataAdapter da = new SqlDataAdapter(cmd);

                cmd.Parameters.AddWithValue("@group_uid", roomDTO.group_uid);
                cmd.Parameters.AddWithValue("@name", roomDTO.name);
                cmd.Parameters.AddWithValue("@u1", roomDTO.u1);
                cmd.Parameters.AddWithValue("@u2", roomDTO.u2);
                cmd.Parameters.AddWithValue("@u3", roomDTO.u3);
                cmd.Parameters.AddWithValue("@u4", roomDTO.u4);
                cmd.Parameters.AddWithValue("@min", roomDTO.min);
                cmd.Parameters.AddWithValue("@max", roomDTO.max);
                cmd.Parameters.AddWithValue("@size", roomDTO.size);
                cmd.Parameters.AddWithValue("@is_active", roomDTO.is_active);
                cmd.Parameters.AddWithValue("@c_by", roomDTO.c_by);
                cmd.Parameters.AddWithValue("@c_date", roomDTO.c_date);
                cmd.Parameters.AddWithValue("@u_by", roomDTO.u_by);
                cmd.Parameters.AddWithValue("@u_date", roomDTO.u_date);

                int uid = Convert.ToInt32(cmd.ExecuteScalar());

                return uid;
            }
        }
    }
}
